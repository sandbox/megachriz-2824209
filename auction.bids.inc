<?php

/**
 * @file
 * Page callbacks for bidding form.
 */

use Drupal\auction\Auction as AuctionLib;
use Drupal\auction\Entity\Auction;
use Drupal\auction\Entity\Bid;
use Drupal\auction\Plugin\auction\BidType\BidAmountInterface;

/**
 * Form callback for placing a bid.
 *
 * @param \Drupal\auction\Entity\Auction $auction
 *   An auction object.
 */
function auction_bids_form($form, &$form_state, $auction) {
  global $user;
  $controller = AuctionLib::getController();

  $form_state['auction']['auction_id'] = $auction->identifier();
  $form_state['auction']['minimum'] = $controller->getMinimumBidAmount($auction);
  $form_state['auction']['current_price'] = $auction->getCurrentPrice();
  $form_state['auction']['currency'] = AuctionLib::getSettings('currency');

  // Check if there is a pending bid in the form state.
  if (isset($form_state['bid'])) {
    $form['#attached']['js'][] = drupal_get_path('module', 'auction') . '/js/auction.js';
    $form['#submit'] = array('auction_bids_form_confirm_submit');
    return AuctionLib::getBidTypePlugin($form_state['bid']->type, $auction)
      ->confirmBidForm($form, $form_state);
  }

  // Use #auction-bids-form as unique jump target.
  $form['#id'] = drupal_html_id('auction_bids_form');

  if ($controller->isBiddingActive($auction)) {
    $form['current_price'] = array(
      '#type' => 'item',
      '#title' => t('Current price'),
      '#markup' => AuctionLib::formatPrice($form_state['auction']['current_price'], $form_state['auction']['currency']),
    );

    if ($auction->userIsWinning($user->uid)) {
      $highest_bid = $auction->getHighestBidAmount();
      $title = t('Your new bid');
      $description = t('Your latest bid is @amount', array('@amount' => $highest_bid));
    }
    else {
      $title = t('Your bid');
      $description = '';
    }
    $form['bid_amount'] = array(
      '#type' => 'textfield',
      '#title' => $title,
      '#default_value' => $form_state['auction']['minimum'],
      '#required' => TRUE,
      '#field_suffix' => $form_state['auction']['currency'],
      '#size' => 10,
      '#element_validate' => array('auction_bids_bid_amount_validate'),
      '#description' => $description,
    );

    $form['actions'] = array(
      '#type' => 'actions',
    );
  }

  foreach (AuctionLib::getAllBidTypePlugins($auction) as $plugin) {
    $plugin->bidForm($form, $form_state);
  }

  return $form;
}

/**
 * Validation function of bidding form.
 */
function auction_bids_bid_amount_validate(&$element, &$form_state, &$form) {
  if (isset($form_state['triggering_element']['#bid_type'])) {
    $auction = entity_load_single('auction', $form_state['auction']['auction_id']);
    $plugin = AuctionLib::getBidTypePlugin($form_state['triggering_element']['#bid_type'], $auction);
    if ($plugin instanceof BidAmountInterface) {
      $plugin->bidAmountValidate($element, $form_state, $form);
    }
  }
}

/**
 * Submit handler for form 'auction_bids_form'.
 */
function auction_bids_form_validate(&$form, &$form_state) {
  if (isset($form_state['triggering_element']['#bid_type'])) {
    $auction = entity_load_single('auction', $form_state['auction']['auction_id']);
    AuctionLib::getBidTypePlugin($form_state['triggering_element']['#bid_type'], $auction)
      ->bidFormValidate($form, $form_state);
  }
}

/**
 * Submit handler for form 'auction_bids_form'.
 */
function auction_bids_form_submit(&$form, &$form_state) {
  if (isset($form_state['triggering_element']['#bid_type'])) {
    $auction = entity_load_single('auction', $form_state['auction']['auction_id']);
    AuctionLib::getBidTypePlugin($form_state['triggering_element']['#bid_type'], $auction)
      ->bidFormSubmit($form, $form_state);
  }
}

/**
 * Submit handler for confirming a bid.
 */
function auction_bids_form_confirm_submit(&$form, &$form_state) {
  $auction_id = $form_state['auction']['auction_id'];
  $lock_name = 'auction_bids_form_confirm_submit_' . $auction_id;
  if (AuctionLib::lockAcquire($lock_name)) {
    $auction = entity_load_single('auction', $auction_id);
    AuctionLib::getBidTypePlugin($form_state['bid']->type, $auction)
      ->confirmBidFormSubmit($form, $form_state);

    lock_release($lock_name);
  }
  else {
    drupal_set_message(t('Could not process your bid because of heavy traffic on the site. Please try again.'), 'error');
  }
}

/**
 * Form callback for removing a buying order.
 *
 * @param \Drupal\auction\Entity\Auction $auction
 *   An auction object.
 * @param int $buying_order_id
 *   A buying order ID.
 */
function auction_bids_remove_buying_order_confirm_form($form, &$form_state, Auction $auction, $buying_order_id) {
  $buying_order = entity_load_single('auction_bid', $buying_order_id);
  // Reasons to skip out early.
  if (!($buying_order instanceof Bid) || $buying_order->type != 'buying_order' || !$buying_order->isHighestBid()) {
    // No buying order.
    auction_bids_remove_buying_order_check($buying_order);

    if (isset($_GET['destination'])) {
      drupal_goto();
    }
    return drupal_access_denied();
  }

  // Keep track of auction and buying order.
  $form_state['auction']['auction_id'] = $auction->identifier();
  $form_state['auction']['bid_id'] = $buying_order->identifier();

  $variables = array(
    '!amount' => '<span class="amount">' . $buying_order->formatPrice() . '</span>',
    '@auction' => $auction->getParentNode()->title,
  );

  $question = t('Remove buying order for @auction', $variables);
  $description = t('Are you sure you want to remove your buying order of !amount for @auction?', $variables);
  $path = current_path();

  return confirm_form($form, $question, $path, $description, NULL, NULL, 'confirm_' . $buying_order->getAuction()->identifier());
}

/**
 * Submit handler for removing a buying order.
 */
function auction_bids_remove_buying_order_confirm_form_submit($form, &$form_state) {
  $auction = entity_load_single('auction', $form_state['auction']['auction_id']);

  // Make sure that the buying order to remove is still the highest.
  $buying_order = $auction->getHighestBid('buying_order');
  if (!$buying_order || $buying_order->bid_id != $form_state['auction']['bid_id']) {
    auction_bids_remove_buying_order_check($buying_order);
    return;
  }

  // Go ahead and remove the buying order now!
  $buying_order->delete();

  // And degrade the latest bid of this customer to just a normal bid or else
  // the customer may receive a mail about his/her buying order being outbid.
  $bid = $auction->getHighestBid('standard');
  if ($bid && $bid->uid == $buying_order->uid) {
    $bid->source = 'standard';
    $bid->save();
  }

  drupal_set_message(t('Your buying order has been removed.'));
}

/**
 * Outputs an error message in case the buying order has issues or does not
 * exist at all.
 *
 * @param \Drupal\auction\Entity\Bid $buying_order
 *   (optional) The buying order to check.
 */
function auction_bids_remove_buying_order_check($buying_order = NULL) {
  if ($buying_order instanceof Bid) {
    if ($buying_order->type != 'buying_order' || !$buying_order->isHighestBid()) {
      drupal_set_message(t('Could not remove your buying order, because new bids have been made in the mean time.'), 'warning');
      return;
    }
  }
  else {
    drupal_set_message(t('Could not remove your buying order, because it seems to be that your buying order is already removed.'), 'warning');
  }
}

/**
 * Return links to bidders profiles.
 *
 * @param array $variables
 * @return string
 */
function theme_auction_bids_overview($variables) {
  $output = '';
  foreach ($variables['bids'] as $bid) {
    $output .= auction_bids_format_bid_overview_row($bid) . "\n";
  }
  return $output;
}

/**
 * Returns human-readable information about single bid.
 *
 * @param stdClass $bid
 * @return string
 */
function auction_bids_format_bid_overview_row($bid) {
  $name = db_select('users', 'u')
    ->fields('u', array('name'))
    ->condition('uid', $bid->uid)
    ->execute()
    ->fetchColumn();

  $output = AuctionLib::formatPrice($bid->amount / 100, $bid->currency_code) . ' - ' . l($name, 'user/' . $bid->uid) . ' - ';

  $output .= $bid->type == Bid::TYPE_STANDARD ? 'standard bid' : 'instant buy';
  return $output;
}
