<?php

/**
 * @file
 * Tokens for auctions.
 */

use \Drupal\auction\Auction as AuctionLib;

/**
 * Implements hook_token_info().
 */
function auction_token_info() {
  $info = array();

  // Auction tokens.
  $info['tokens']['auction']['current_price'] = array(
    'name' => t('Current price'),
    'description' => t('Auction current price.'),
  );
  $info['tokens']['auction']['link'] = array(
    'name' => t('Link'),
    'description' => t('Link to auction node.'),
  );
  $info['tokens']['auction']['bids_overview'] = array(
    'name' => t('Bids overview'),
    'description' => t('Bidders profiles links sorted by highest bid.'),
  );

  // Auction bid tokens.
  $info['tokens']['auction_bid']['amount-formatted'] = array(
    'name' => t('Amount (formatted)'),
    'description' => t('The formatted amount with currency code.'),
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function auction_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  }
  else {
    $language_code = NULL;
  }

  if (!empty($data[$type])) {
    switch ($type) {
      case 'auction':
        $auction = $data['auction'];
        foreach ($tokens as $name => $original) {
          switch ($name) {
            case 'current_price':
              $replacements[$original] = AuctionLib::formatPrice($auction->current_price, $auction->currency_code);
              break;

            case 'link':
              $nid = AuctionLib::getController()
                ->getParentEntityWrapper($auction->auction_id)
                ->nid
                ->value();

              $options = array(
                'absolute' => TRUE,
              ) + $url_options;

              $replacements[$original] = url('node/' . $nid, $options);
              break;

            case 'bids_overview':
              $bids_overview = array(
                '#theme' => 'auction_bids_overview',
                '#bids' => $auction->bids,
              );
              $replacements[$original] = render($bids_overview);
              break;
          }
        }
        break;

      case 'auction_bid':
        $bid = $data['auction_bid'];
        if (isset($tokens['amount-formatted'])) {
          $replacements[$tokens['amount-formatted']] = AuctionLib::formatPrice($bid->amount / 100, $bid->currency_code);
        }
        break;
    }
  }

  return $replacements;
}
