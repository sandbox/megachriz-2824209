<?php

/**
 * Jul 20, 2012
 * @file auction.rules_defaults.inc
 * @author blazey http://drupal.org/user/353861
 */

/**
 * Implements hook_default_rules_configuration().
 */
function auction_default_rules_configuration() {
  $configs = array();

  // Allow a simple way to disable all rules.
  $active = variable_get('auction_rules_active', TRUE);

  $rule = rules_reaction_rule();
  $rule->label = 'Auction: started: notify author';
  $rule->active = $active;
  $rule->tags = array('auction');
  $rule->event('auction_started')
    ->action('mail', array(
      'to' => '[auction:author:mail]',
      'subject' => 'Your auction has been started.',
      'message' => 'Your auction has started. You can check it at [auction:url].',
      'language' => '[auction:author:language]',
    ));

  $configs['rules_auction_start_notify_author'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = 'Auction: finished: notify author';
  $rule->active = $active;
  $rule->tags = array('auction');
  $rule->event('auction_finished')
    ->action('mail', array(
      'to' => '[auction:author:mail]',
      'subject' => 'Your auction has finished.',
      'message' => 'Your auction has just finished. You can check it at [auction:url].',
      'language' => '[auction:author:language]',
    ));

  $configs['rules_auction_finished_notify_author'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = 'Auction: bid placed: notify author';
  $rule->active = $active;
  $rule->tags = array('auction', 'auction_bid');
  $rule->event('auction_bid_placed')
    ->condition('data_is', array(
      'data:select' => 'bid:type',
      'value' => 'standard',
    ))
    ->action('mail', array(
      'to' => '[auction:author:mail]',
      'subject' => 'Bid placed in your auction.',
      'message' => 'Bid has been placed in your auction. You can check it at [auction:url].',
      'language' => '[auction:author:language]',
    ));

  $configs['rules_auction_bid_placed_notify_author'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = 'Auction: bid placed: notify bidder';
  $rule->active = $active;
  $rule->tags = array('auction', 'auction_bid');
  $rule->event('auction_bid_placed')
    ->condition('data_is', array(
      'data:select' => 'bid:type',
      'value' => 'standard',
    ))
    ->condition('data_is', array(
      'data:select' => 'bid:source',
      'op' => 'IN',
      'value' => array(
        'standard',
        'buying_order_new',
      ),
    ))
    ->action('mail', array(
      'to' => '[bidder:mail]',
      'subject' => 'Your bid has been placed',
      'message' => "Dear Sir\/Madam,\r\n\r\nYou placed a bid of [bid:amount-formatted] on [auction-node:title].\r\nYou will be notified in case a higher bid is placed.\r\n\r\nYours sincerely,\r\n\r\n--  [site:name] team",
      'language' => '[bidder:language]',
    ));

  $configs['rules_auction_bid_placed_notify_bidder'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = 'Auction: automatic bid placed by buying order: notify bidder';
  $rule->active = $active;
  $rule->tags = array('auction', 'auction_bid');
  $rule->event('auction_bid_placed')
    ->condition('data_is', array(
      'data:select' => 'bid:type',
      'value' => 'standard',
    ))
    ->condition('data_is', array(
      'data:select' => 'bid:source',
      'value' => 'buying_order',
    ))
    ->action('mail', array(
      'to' => '[bidder:mail]',
      'subject' => 'A new bid was placed',
      'message' => "Dear Sir\/Madam,\r\n\r\nThrough your buying order an automatic bid of [bid:amount-formatted] on [auction-node:title] has been placed.\r\nYou will be notified in case a higher bid is placed.\r\n\r\nYours sincerely,\r\n\r\n--  [site:name] team",
      'language' => '[bidder:language]',
    ));

  $configs['rules_auction_bid_placed_auto_notify_bidder'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = 'Auction: buying order placed: notify bidder';
  $rule->active = $active;
  $rule->tags = array('auction', 'auction_buying_order');
  $rule->event('auction_bid_placed')
    ->condition('data_is', array(
      'data:select' => 'bid:type',
      'value' => 'buying_order',
    ))
    ->action('mail', array(
      'to' => '[bidder:mail]',
      'subject' => 'Your buying order has been placed',
      'message' => "Dear Sir\/Madam,\r\n\r\nYou placed a buying order of [bid:amount-formatted] on [auction-node:title].\r\nYou are always kept informed about new bids.\r\n\r\nYours sincerely,\r\n\r\n--  [site:name] team",
      'language' => '[bidder:language]',
    ));

  $configs['rules_auction_buying_order_placed_notify_bidder'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = 'Auction: bid placed: notify outbid user';
  $rule->active = $active;
  $rule->tags = array('auction', 'auction_bid', 'auction_outbid');
  $rule->event('auction_bid_placed')
    ->condition('data_is', array(
      'data:select' => 'bid:type',
      'value' => 'standard',
    ))
    ->condition(
      rules_condition('data_is_empty', array(
        'data:select' => 'outbid-user',
      ))->negate()
    )
    ->condition(
      rules_condition('data_is_empty', array(
        'data:select' => 'outbid-bid',
      ))->negate()
    )
    ->condition('data_is', array(
      'data:select' => 'outbid-bid:source',
      'value' => 'standard',
    ))
    ->action('mail', array(
      'to' => '[outbid-user:mail]',
      'subject' => 'You have been outbid',
      'message' => "Dear Sir\/Madam,\r\n\r\nThe bid that you placed on [auction-node:title] has been outbid.\r\nTo place a higher bid, go to [auction:url].\r\n\r\nYours sincerely,\r\n\r\n--  [site:name] team",
      'language' => '[outbid-user:language]',
    ));

  $configs['rules_auction_bid_placed_notify_outbid_user'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = 'Auction: bid placed: notify outbid user (buying order)';
  $rule->active = $active;
  $rule->tags = array('auction', 'auction_bid', 'auction_outbid');
  $rule->event('auction_bid_placed')
    ->condition('data_is', array(
      'data:select' => 'bid:type',
      'value' => 'standard',
    ))
    ->condition(
      rules_condition('data_is_empty', array(
        'data:select' => 'outbid-user',
      ))->negate()
    )
    ->condition(
      rules_condition('data_is_empty', array(
        'data:select' => 'outbid-bid',
      ))->negate()
    )
    ->condition('data_is', array(
      'data:select' => 'outbid-bid:source',
      'op' => 'IN',
      'value' => array(
        'buying_order',
        'buying_order_new',
      ),
    ))
    ->action('mail', array(
      'to' => '[outbid-user:mail]',
      'subject' => 'Your buying order has been outbid',
      'message' => "Dear Sir\/Madam,\r\n\r\nThe buying order that you placed on [auction-node:title] has been outbid.\r\nThe current bid exceeds that of your buying order. To place a higher bid, go to [auction:url].\r\n\r\nYours sincerely,\r\n\r\n--  [site:name] team",
      'language' => '[outbid-user:language]',
    ));

  $configs['rules_auction_bid_placed_notify_outbid_user_buying_order'] = $rule;

  return $configs;
}
