<?php

namespace Drupal\auction_commerce;

use \DrupalQueue;
use \Drupal\auction\Auction as AuctionLib;
use \Drupal\auction\Entity\Auction;

/**
 * API functions for Auction Commerce.
 */
abstract class AuctionCommerce {
  /**
   * The initial order status for a auction commerce order.
   *
   * @var string
   */
  const INITIAL_ORDER_STATUS = 'checkout_checkout';

  /**
   * Queues processing a finished auction.
   *
   * @param Drupal\auction\Entity\Auction $auction
   *   The finished auction.
   * @param int $uid
   *   The user that won the auction.
   */
  public static function queueFinishedAuction(Auction $auction, $uid) {
    $queue = DrupalQueue::get('auction_commerce_finished');
    $queue->createItem(array(
      'auction' => $auction,
      'uid' => $uid,
    ));
  }

  /**
   * Processes a finished auction.
   */
  public static function processFinishedAuction($item) {
    $auction = $item['auction'];
    $uid = $item['uid'];

    $lock_name = 'auction_commerce_auction_finished_' . $uid;
    if (!AuctionLib::lockAcquire($lock_name)) {
      // Failed to put product on order. Queue task.
      static::queueFinishedAuction($auction, $uid);
      return;
    }

    // Auction is sold. Create order for winning user.
    $order_wrapper = static::createOrderForUser($uid);

    // Create product instance representing auction.
    $product = static::createAuctionProduct($auction);

    // Check if order already contains product.
    if (!static::orderContainsProduct($order_wrapper->value(), $product)) {
      // Add line item representing auction.
      $auction_line_item = commerce_product_line_item_new($product, 1, $order_wrapper->order_id->value());
      commerce_line_item_save($auction_line_item);
      $order_wrapper->commerce_line_items[] = $auction_line_item;
    }

    // Save order.
    $order_wrapper->save();
    rules_invoke_all('auction_bill_created', $auction, $order_wrapper->raw());
    lock_release($lock_name);
  }

  /**
   * Creates commerce product of type auction.
   *
   * @param Drupal\auction\Entity\Auction $auction
   *   The auction to create a product for.
   * @return stdClass
   */
  public static function createAuctionProduct(Auction $auction) {
    $controller = AuctionLib::getController();

    return static::createProduct('auction_' . $auction->auction_id, $auction->current_price, array(
      'title' => $controller->getParentEntityWrapper($auction->auction_id)->title->value(),
      'uid' => $auction->uid,
      'auction_id' => $auction->auction_id,
    ));
  }

  /**
   * Creates commerce product to represent auction related payments.
   *
   * @param string $sku
   *   The product SKU.
   * @param float $price
   *   The product price.
   * @param array $values
   *   (optional) Other product values.
   *
   * @return stdClass
   */
  public static function createProduct($sku, $price, array $values = array()) {
    global $user;

    // Default values.
    $values += array(
      'type' => 'auction',
      'title' =>'',
      'uid' => $user->uid,
      'currency' => commerce_default_currency(),
      'auction_id' => NULL,
    );

    // Make sure no product exist with the same SKU. Else use that product instead.
    $product = commerce_product_load_by_sku($sku);
    if (!$product) {
      $product = commerce_product_new($values['type']);
    }
    $product_wrapper = entity_metadata_wrapper('commerce_product', $product);
    $product_wrapper->title = $values['title'];
    $product_wrapper->uid = $values['uid'];
    $product_wrapper->sku = $sku;
    $product_wrapper->commerce_price = array(
      'amount' => $price,
      'currency_code' => $values['currency'],
      'data' => array('components' => array()),
    );

    if ($values['type'] == 'auction') {
      $product_wrapper->field_auction = $values['auction_id'];
    }

    $product_wrapper->save();

    return $product_wrapper->raw();
  }

  /**
   * Creates an order for given user.
   *
   * @param int $uid
   *   The ID of the user to create an order for.
   * @param string $type
   *   (optional) The type of order to create.
   *   Defaults to 'commerce_order'.
   *
   * @return EntityMetadataWrapper
   *   A wrapped order object.
   */
  public static function createOrderForUser($uid, $type = 'commerce_order') {
    $orders = &drupal_static(__METHOD__);
    if (isset($orders[$uid])) {
      return entity_metadata_wrapper('commerce_order', $orders[$uid]);
    }

    // Make sure no existing order exists for user.
    $order = commerce_cart_order_load($uid);
    if (!$order) {
      $order = commerce_order_new($uid, static::INITIAL_ORDER_STATUS);
      $order->type = $type;
      commerce_order_save($order);

      // Reset static cache for commerce_cart_order_id().
      drupal_static_reset('commerce_cart_order_id');
    }

    // Add order to static $orders variable.
    $orders[$uid] = $order;

    // Return a wrapper for the commerce order.
    return entity_metadata_wrapper('commerce_order', $order);
  }

  /**
   * Checks if the given order contains the given product.
   *
   * @param object $order
   *   A commerce_order object.
   * @param object $product
   *   A commerce_product object.
   *
   * @return bool
   *   True if the order contains the given product.
   *   False otherwise.
   */
  public static function orderContainsProduct($order, $product) {
    $line_items = field_get_items('commerce_order', $order, 'commerce_line_items');
    if (!empty($line_items)) {
      foreach ($line_items as $delta => $line_item_entry) {
        if ($line_item = commerce_line_item_load($line_item_entry['line_item_id'])) {
          $product_field = field_get_items('commerce_line_item', $line_item, 'commerce_product');
          if ($product_field[0]['product_id'] == $product->product_id) {
            return TRUE;
          }
        }
      }
    }

    return FALSE;
  }

  /**
   * Return fee ranges for auction creators.
   */
  public static function getFeeRanges() {
    $ranges = module_invoke_all('commerce_auction_fee_ranges');
    drupal_alter('commerce_auction_fee_ranges', $ranges);
    return $ranges;
  }

  /**
   * Return fee range for given amount.
   *
   * @param int $monthly_sell_amount
   * @return array
   */
  public static function determineFeeRange($monthly_sell_amount) {
    $range = array();
    foreach (static::getFeeRanges() as $value) {
      if ($monthly_sell_amount < $value['from']) {
        break;
      }
      $range = $value;
    }
    return $range;
  }

  /**
   * Creates bills for auctions creators.
   *
   * @param int $current_month
   */
  public static function makeMonthlyBills($current_month) {
    // Determine last month's number and year. Special case is January.
    $current_year = date('Y');
    if ($current_month == 1) {
      $last_month = 12;
      $last_year = date('Y') - 1;
    } else {
      $last_month = $current_month - 1;
      $last_year = $current_year;
    }

    // Split auctions finished last month by creator uid.
    $auctions_by_user = array();
    foreach (static::getAuctionFinishedLastMonth($current_month, $current_year, $last_month, $last_year) as $auction) {
      $auctions_by_user[$auction->uid][] = $auction;
    }

    // Add separate cron queue item for every user so we are sure everything
    // gets processed.
    $queue = DrupalQueue::get('static::billing');
    foreach ($auctions_by_user as $uid => $auctions) {
      $queue->createItem(array(
        'uid' => $uid,
        'auctions' => $auctions,
        'month' => $last_month,
        'year' => $last_year,
      ));
    }
  }

  /**
   * Return auctions that finished last month
   *
   * @param int $current_month
   * @return array
   */
  public static function getAuctionFinishedLastMonth($current_month, $current_year, $last_month, $last_year) {
    // Get timestamps to put in query.
    $from = strtotime("$last_year/$last_month/1 00:00");
    $to = strtotime("$current_year/$current_month/1 00:00");
    $query = db_select('auction', 'a')
      ->fields('a')
      ->condition('status', Auction::STATUS_FINISHED)
      ->condition('changed', $from, '>=')
      ->condition('changed', $to, '<');

    // Select and return auctions.
    return $query
      ->execute()
      ->fetchAllAssoc('auction_id');
  }

  /**
   * Auction commerce billing cron queue callback. Creates monthly bill for a user.
   *
   * @param array $data
   */
  public static function makeMonthlyOrderForAuctionCreator($data) {
    $uid = $data['uid'];
    $auctions = $data['auctions'];

    // Create monthly bill order.
    $order_wrapper = static::createOrderForUser($uid, 'auction_monthly_bill');

    // Calculate needed amounts.
    $sum = static::sumAmounts($auctions);
    $fee_range = static::determineFeeRange($sum / 100);
    $provision = $sum * $fee_range['provision'];

    // Create product representing provision.
    $sku = implode('_', array('auction_provision', $uid, $data['year'], $data['month']));
    $provision_product = static::createProduct($sku, $provision, array(
      'type' => 'auction_fee',
      'title' => t('Monthly provision: @sum * @provision', array('@sum' => $sum / 100, '@provision' => $fee_range['provision'])),
      'uid' => $uid,
    ));
    // Add line item.
    $provision_line_item = commerce_product_line_item_new($provision_product, 1, $order_wrapper->order_id->value());
    commerce_line_item_save($provision_line_item);
    $order_wrapper->commerce_line_items[] = $provision_line_item;

    // Create product representing fee for auctions.
    $quantity = count($auctions);
    $sku = implode('_', array('auction_fee', $uid, $data['year'], $data['month']));
    $price = $fee_range['single_auction_fee'] * 100;
    $fee_product = static::createProduct($sku, $price, array(
      'type' => 'auction_fee',
      'title' => t('Fee for auctions finished @year-@month', array('@year' => $data['year'], '@month' => $data['month'])),
      'uid' => $uid,
    ));
    // Add line item.
    $fee_line_item = commerce_product_line_item_new($fee_product, $quantity, $order_wrapper->order_id->value());
    commerce_line_item_save($fee_line_item);
    $order_wrapper->commerce_line_items[] = $fee_line_item;

    $order_wrapper->save();
  }

  /**
   * Returns sum of auctions amounts.
   *
   * @param array $auctions
   * @return int
   */
  public static function sumAmounts($auctions) {
    $sum = 0;
    foreach ($auctions as $auction) {
      $sum += $auction->current_price;
    }
    return $sum;
  }
}
