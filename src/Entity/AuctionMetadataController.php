<?php

namespace Drupal\auction\Entity;

use EntityDefaultMetadataController;

/**
 * Meta data controller class for auction entity.
 */
class AuctionMetadataController extends EntityDefaultMetadataController {

  /**
   * {@inheritdoc}
   */
  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();

    // Add meta-data about auction properties.
    $properties = &$info['auction']['properties'];

    $properties['author'] = array(
      'label' => t("Author"),
      'type' => 'user',
      'description' => t("The author of the auction."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer auctions',
      'required' => TRUE,
      'schema field' => 'uid',
    );
    $properties['current_price_decimal'] = array(
      'label' => t('Current price with decimals'),
      'type' => 'decimal',
      'description' => t('Current price as seen in auction nodes'),
      'getter callback' => 'auction_entity_get_current_price',
      'required' => FALSE,
    );
    // It's not used now. Consider removing.
    $properties['winning_user'] = array(
      'label' => t("Winning user"),
      'type' => 'user',
      'description' => t("The user that is currently winning in auction."),
      'getter callback' => 'auction_entity_get_winning_user',
      'required' => FALSE,
    );
    $properties['bids'] = array(
      'label' => t('Bids'),
      'type' => 'list<auction_bid>',
      'getter callback' => 'auction_entity_get_bids',
    );
    $properties['highest_bid'] = array(
      'label' => t('Highest bid'),
      'type' => 'auction_bid',
      'getter callback' => 'auction_entity_get_highest_bid',
    );

    return $info;
  }

}
