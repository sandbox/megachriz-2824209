<?php

namespace Drupal\auction\Entity;

use \DatabaseTransaction;
use \EntityAPIController;
use Drupal\auction\Auction as AuctionLib;

/**
 * Entity controller class for entity type 'auction_bid'.
 */
class BidController extends EntityAPIController {
  /**
   * Saves a bid.
   *
   * @param Drupal\auction\Entity\Bid $bid
   *   The bid to save.
   * @param DatabaseTransaction $transaction
   *   An optional transaction object.
   *
   * @return
   *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
   */
  public function save($bid, DatabaseTransaction $transaction = NULL) {
    $bid->changed = REQUEST_TIME;
    $is_new = $bid->is_new = empty($bid->{$this->idKey});

    if (empty($bid->{$this->idKey}) || !empty($bid->is_new)) {
      // Set the creation timestamp if not set, for new entities.
      if (empty($bid->created)) {
        $bid->created = REQUEST_TIME;
      }
    }

    if (empty($bid->uid)) {
      global $user;
      $bid->uid = $user->uid;
    }

    // Save now.
    $result = parent::save($bid, $transaction);

    // Make sure that the bid is kept in the cache of the controller.
    $this->cacheSet(array(
      $bid->identifier() => $bid,
    ));

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function invoke($hook, $entity) {
    parent::invoke($hook, $entity);

    switch ($hook) {
      case 'delete':
        // Update price.
        $auction = $entity->getAuction();
        AuctionLib::getController()->updateCurrentPrice($auction);
        break;
    }
  }

}
