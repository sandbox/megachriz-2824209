<?php

namespace Drupal\auction\Entity;

use \DatabaseTransaction;
use \EntityAPIController;
use \EntityFieldQuery;
use \stdClass;
use \Drupal\auction\Auction as AuctionLib;
use \Drupal\auction\Entity\Auction;
use \Drupal\auction\Exception\InvalidBidException;

/**
 * Entity controller class for entity type 'auction'.
 */
class AuctionEntityController extends EntityAPIController {

  /**
   * Create a generic auctiln.
   *
   * @param array $values
   *   An array of values to set, keyed by property name.
   * @return
   *   An auction object with all default fields initialized.
   */
  public function create(array $values = array()) {
    global $user;
    $values += array(
      'auction_id' => '',
      'is_new' => TRUE,
      'uid' => $user->uid,
      'current_price' => 0,
      'currency_code' => AuctionLib::getSettings('currency'),
      'type' => 'auction',
      'relisted' => 0,
      'bids' => array(),
      'changed' => '',
      'status' => Auction::STATUS_NOT_YET_STARTED,
    );

    return parent::create($values);
  }

  /**
   * Builds a structured array representing the entity's content.
   *
   * The content built for the entity will vary depending on the $view_mode
   * parameter.
   *
   * @param $entity
   *   An entity object.
   * @param $view_mode
   *   View mode, e.g. 'full', 'teaser'...
   * @param $langcode
   *   (optional) A language code to use for rendering. Defaults to the global
   *   content language of the current request.
   * @return
   *   The renderable array.
   */
  public function buildContent($auction, $view_mode = 'full', $langcode = NULL, $content = array()) {
    module_load_include('inc', 'auction', 'auction.bids');
    $content = parent::buildContent($auction, $view_mode, $langcode, $content);
    $content['#attached']['css'][] = drupal_get_path('module', 'auction') . '/css/auction.view.css';

    if (module_exists('jquery_countdown')) {
      $content['countdown'] = array(
        '#type' => 'item',
        '#title' => t('Auction ends in'),
        '#markup' => '<div id="countdown-' . $auction->auction_id . '"></div>',
        '#weight' => -15,
        '#access' => $auction->status == Auction::STATUS_ACTIVE,
      );
      jquery_countdown_add("#countdown-" . $auction->auction_id, array("until" => $this->getSecondsUntilEnd($auction), "onExpiry" => "auctionEnded"));
      drupal_add_js("function auctionEnded() { window.location.href = window.location.href; }", 'inline');
    }

    if (!empty($auction->field_auction_rate_card_price[LANGUAGE_NONE][0]['value'])) {
      $content['rate_card_price'] = array(
        '#type' => 'item',
        '#title' => t('Rate card price'),
        '#markup' => AuctionLib::formatPrice($auction->field_auction_rate_card_price[LANGUAGE_NONE][0]['value'], $auction->currency_code),
      );
    }

    if (user_access('view bids')) {
      $view = views_get_view('auction_bids');
      $history = $view->preview(NULL, array($auction->auction_id));
      $content['bids'] = array(
        '#theme' => 'auction_bids',
        '#auction' => $auction,
      );
    }
    else {
      $history = '';
    }

    switch ($auction->getStatus()) {
      case Auction::STATUS_NOT_YET_STARTED:
        $content['bids']['form'] = array(
          '#markup' => t('Auction is inactive.'),
        );
        break;

      case Auction::STATUS_ACTIVE:
        $content['bids']['history'] = $history;

        // Attach bids form if user has access.
        if (auction_entity_access('bid', $auction)) {
          $form = drupal_get_form('auction_bids_form', $auction, $view_mode, $langcode);
          $content['bids']['form'] = $form;
        }
        else {
          $content['bids']['form'] = array(
            '#theme' => 'auction_bid_forbidden',
            '#auction' => $auction,
          );
        }
        break;

      case Auction::STATUS_FINISHED:
        $content['bids']['history'] = $history;
        $content['bids']['form'] = array(
          '#markup' => t('Auction has finished.'),
        );
        break;
    }

    return $content;
  }

  /**
   * Implements EntityAPIControllerInterface.
   *
   * @param $transaction
   *   Optionally a DatabaseTransaction object to use. Allows overrides to pass
   *   in their transaction object.
   */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    parent::delete($ids, $transaction);
    foreach ($ids as $auction_id) {
      db_delete('auction_bids')->condition('auction_id', $auction_id)->execute();
    }
  }

  /**
   * Saves an auction.
   *
   * @param Drupal\auction\Entity\Auction $auction
   *   The full auction object to save.
   * @param DatabaseTransaction $transaction
   *   An optional transaction object.
   *
   * @return
   *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
   */
  public function save($auction, DatabaseTransaction $transaction = NULL) {
    $auction->changed = REQUEST_TIME;

    $auction->is_new = empty($auction->auction_id);

    if (empty($auction->{$this->idKey}) || !empty($auction->is_new)) {
      // Set the creation timestamp if not set, for new entities.
      if (empty($auction->created)) {
        $auction->created = REQUEST_TIME;
      }
    }

    if (empty($auction->uid)) {
      global $user;
      $auction->uid = $user->uid;
    }

    if (!isset($auction->status)) {
      $auction->status = Auction::STATUS_NOT_YET_STARTED;
    }

    if (!isset($auction->bids)) {
      $auction->bids = array();
    }

    if ($auction->status == Auction::STATUS_FINISHED) {
      $auction->current_price = $this->determineFinishedAuctionPrice($auction) * 100;
    }
    else {
      $auction->current_price = $this->determineCurrentPrice($auction->getBids('standard'), $auction->getStartingPrice()) * 100;
    }

    $auction->currency_code = empty($auction->bids) ? AuctionLib::getSettings('currency') : $auction->getLastBid()->currency_code;

    $result = parent::save($auction, $transaction);

    // Make sure that the auction is kept in the cache of the controller.
    $this->cacheSet(array(
      $auction->identifier() => $auction,
    ));

    return $result;
  }

  /**
   * Overridden.
   * @see EntityAPIController#load($ids, $conditions)
   */
  public function load($ids = array(), $conditions = array()) {
    $auctions = parent::load($ids, $conditions);
    foreach ($auctions as &$auction) {
      if (isset($auction->auction_id)) {
        $this->attachBids($auction);
        $auction = $this->changeStatusIfNeeded($auction);
      }
    }
    return $auctions;
  }

  /**
   * Attaches bids to auction entity.
   *
   * @param Drupal\auction\Entity\Auction $auction
   */
  public function attachBids(Auction $auction) {
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'auction_bid')
      ->propertyCondition('auction_id', $auction->auction_id)
      ->propertyOrderBy('amount', 'DESC')
      ->propertyOrderBy('created', 'ASC')
      ->propertyOrderBy('bid_id', 'ASC')
      ->execute();

    if (!empty($result['auction_bid'])) {
      $bid_ids = array_keys($result['auction_bid']);
      $auction->bids = entity_load('auction_bid', $bid_ids);
    }
    else {
      $auction->bids = array();
    }
  }

  /**
   * Saves a bid.
   *
   * @param Drupal\auction\Entity\Bid $bid
   *   A bid object.
   * @param bool $notify
   *   (optional) If the bid may result into a notification.
   *   Defaults to TRUE.
   *
   * @return bool
   *   If saving was successful.
   */
  public function saveBid(Bid $bid, $notify = TRUE) {
    $auction = $bid->getAuction();

    $bid_is_new = $bid->isNew();
    $bid->save();

    // Eventually extend time of auction when a bid was placed in the last minutes.
    if (variable_get('auction_extend', TRUE)) {
      $expiration_timestamp = $auction->field_auction_date[LANGUAGE_NONE][0]['value2'];
      $time_period = variable_get('auction_final_period', 15) * 60;
      $extension_time = variable_get('auction_extension_time', 30) * 60;
      if (
        $expiration_timestamp - REQUEST_TIME < $time_period
        && $expiration_timestamp - REQUEST_TIME > 0
      ) {
        $auction->field_auction_date[LANGUAGE_NONE][0]['value2'] = $extension_time + $expiration_timestamp;
        $auction->extended = TRUE;
        $auction->save();
      }
    }

    $this->updateCurrentPrice($auction);

    // Invoke hooks for each active bid type plugin.
    foreach (AuctionLib::getAllBidTypePlugins($auction) as $plugin) {
      $plugin->onSaveBid($bid, $bid_is_new);
    }

    // After the plugin hooks are invoked, the highest bid could have changed.
    // Only invoke the rule if the given bid is still the highest one.
    // @todo figure out why.
    if ($bid === $auction->getHighestBid($bid->type) && $notify) {
      $this->notifyPlacedBid($auction, $bid);
    }

    return TRUE;
  }

  /**
   * Invokes a rule when a bid has been placed.
   *
   * @param Drupal\auction\Entity\Auction $auction
   *   The auction for which a bid was placed.
   * @param Drupal\auction\Entity\Bid $bid
   *   (optional) The bid that was placed.
   */
  public function notifyPlacedBid(Auction $auction, Bid $bid = NULL) {
    if (empty($bid)) {
      $bid = $auction->getHighestBid();
    }

    $outbid_user = NULL;
    $outbid_bid = NULL;

    if ($bid->type == 'standard' || 1) {
      // Get the previous highest bid.
      $outbid_bid = $auction->getPreviousHighestBid();

      // Check if the previous highest bid (if any) belongs to an other user.
      if ($outbid_bid instanceof Bid && $outbid_bid->uid != $bid->uid) {
        $outbid_user = user_load($outbid_bid->uid);
      }
    }

    $auction_node = $auction->getParentNode();
    $bidder = user_load($bid->uid);
    rules_invoke_all('auction_bid_placed', $auction, $bid, $bidder, $outbid_bid, $outbid_user, $auction_node);
  }

  /**
   * Check if auction should change its status and do it if needed.
   *
   * @param Drupal\auction\Entity\Auction $auction
   */
  public function changeStatusIfNeeded(Auction $auction) {
    static $changing = array();
    if (!in_array($auction->auction_id, $changing)) {
      $changing[] = $auction->auction_id;
      $wrapper = entity_metadata_wrapper('auction', $auction);

      if (time() >= $wrapper->field_auction_date->value->value() && $auction->status == Auction::STATUS_NOT_YET_STARTED) {
        return $auction->activate();
      }
      elseif (time() >= $wrapper->field_auction_date->value2->value() && $auction->status == Auction::STATUS_ACTIVE) {
        return $auction->deactivate();
      }
    }
    return $auction;
  }

  /**
   * Returns current price of an auction.
   *
   * @param Drupal\auction\Entity\Auction $auction
   * @return int
   */
  public function updateCurrentPrice(Auction $auction) {
    // Ensure bids are loaded. Even the one added in the same request.
    $this->attachBids($auction);
    $auction->save();
  }

  /**
   * Returns bid step of an auction.
   *
   * @param Drupal\auction\Entity\Auction $auction
   *   The auction to get the bid step for.
   *
   * @return float
   *   The increase amount the bid need to be multiplied with.
   */
  public function getBidStep(Auction $auction) {
    $wrapper = $this->getWrapper($auction);
    $value = $wrapper->field_auction_bid_step->value();

    $value = floatval($value);
    $value = round($value, 2);

    if (!$value) {
      // Allow other modules to specify the bid step.
      $values = module_invoke_all('auction_bid_step', $auction);
      if (count($values)) {
        // If there are multiple modules specifying the bid step, then the last
        // module wins.
        $value = end($values);
      }
    }

    if (!$value) {
      $settings = AuctionLib::getSettings();
      $value = floatval($settings['bid_step']);
      $value = round($value, 2);
    }

    return $value;
  }

  /**
   * Returns lowest bid amount that can be accepted.
   *
   * @param Drupal\auction\Entity\Auction $auction
   * @return int
   */
  public function getMinimumBidAmount(Auction $auction) {
    if (!$auction->hasBids()) {
      // In case of no bids, the start price may be bid.
      return $auction->getCurrentPrice();
    }
    return $auction->getCurrentPrice() + $this->getBidStep($auction);
  }

  /**
   * Returns amount of highest bid placed in auction.
   *
   * @param Drupal\auction\Entity\Auction $auction
   *   The auction to request bid information for.
   * @param int $type
   *   The bid type to filter on.
   *
   * @return stdClass|null
   *   A bid object, if found.
   *   NULL otherwise.
   */
  public function getHighestBid(Auction $auction, $type = NULL) {
    user_error('Call getHighestBid() on auction instead.', E_USER_DEPRECATED);
    return $auction->getHighestBid($type);
  }

  /**
   * Returns amount of highest bid placed in auction.
   *
   * @param Drupal\auction\Entity\Auction $auction
   *   The auction to request bid information for.
   * @param int $type
   *   The bid type to filter on.
   *
   * @return int
   *   A bid amount, if found.
   *   NULL otherwise.
   */
  public function getHighestBidAmount(Auction $auction, $type = NULL) {
    user_error('Call getHighestBidAmount() on auction instead.', E_USER_DEPRECATED);
    return $auction->getHighestBidAmount($type);
  }

  /**
   * Returns number of seconds left until the end of auction.
   *
   * @param Drupal\auction\Entity\Auction $auction
   * @return int
   */
  public function getSecondsUntilEnd(Auction $auction) {
    $wrapper = $this->getWrapper($auction);
    return $wrapper->field_auction_date->value2->value() - time() - 1;
  }

  /**
   * Validates a bid.
   *
   * @param \Drupal\auction\Entity\Auction $auction
   *   The auction to check a bid amount for.
   * @param int $bid_amount
   *   The bid amount to validate.
   *
   * @throws \Drupal\auction\Exception\InvalidBidException
   *   In case of an invalid bid.
   */
  public function validateBid(Auction $auction, $bid_amount) {
    $current_price = $auction->getCurrentPrice();
    $current_minimum = $this->getMinimumBidAmount($auction);
    $highest_bid_amount = $auction->getHighestBidAmount(Bid::TYPE_STANDARD);
    $increase_amount = $bid_amount - $current_minimum;

    if (!$highest_bid_amount) {
      $highest_bid_amount = $current_minimum;
    }

    // Validate minimum bid.
    if ($bid_amount < $current_minimum) {
      throw new InvalidBidException(t('Your bid amount is too low. The minimum bid is @minimum.', array(
        '@minimum' => $auction->formatPrice($current_minimum),
      )));
    }
    else {
      global $user;
      if ($auction->userIsWinning($user->uid)) {
        if ($bid_amount < $highest_bid_amount) {
          throw new InvalidBidException(t('Your bid amount is too low. The minimum bid is @minimum.', array(
            '@minimum' => $auction->formatPrice($highest_bid_amount),
          )));
        }
      }
    }

    // Validate bid in steps.
    $step = $this->getBidStep($auction);
    if ($step && !AuctionLib::isDivisable($increase_amount, $step)) {
      throw new InvalidBidException(t('Your bid amount should increase the current high bid (@highest) by a multiple of @inc.', array(
        '@inc' => $step,
        '@highest' => $auction->formatPrice($current_price),
      )));
    }

    // Allow other modules to validate the bid.
    module_invoke_all('auction_validate_bid', $auction, $bid_amount);
  }

  /**
   * Checks wether or not this auction is in bid mode.
   *
   * @param Drupal\auction\Entity\Auction $auction
   *   The auction to check the bid mode for.
   *
   * @return bool
   *   True if bidding is active.
   *   False otherwise.
   */
  public function isBiddingActive(Auction $auction) {
    $starting_price = $auction->getStartingPrice();
    return !empty($starting_price);
  }

  /**
   * Chcecks wheather minimum price has been reached.
   *
   * @param Drupal\auction\Entity\Auction $auction
   * @return boolean
   */
  public function minimumPriceReached(Auction $auction) {
    return $auction->current_price >= $auction->getMinimumPrice() * 100;
  }

  /**
   * Returns wrapper for given auction.
   *
   * @param Drupal\auction\Entity\Auction $auction
   * @return EntityMetadataWrapper
   */
  public function getWrapper(Auction $auction) {
    return entity_metadata_wrapper('auction', $auction);
  }

  /**
   * Returns parent entity wrapper.
   *
   * @param int $auction_id
   *   The ID of the auction.
   *
   * @return EntityMetadataWrapper
   *   A metadata wrapper for a product if found.
   *   Null otherwise.
   */
  public function getParentEntityWrapper($auction_id) {
    $data = db_select('field_data_field_auction', 'fd')
      ->fields('fd', array('entity_type', 'entity_id'))
      ->condition('field_auction_target_id', $auction_id)
      ->execute()
      ->fetchAssoc();

    if ($data) {
      return entity_metadata_wrapper($data['entity_type'], $data['entity_id']);
    }
  }

  /**
   * Calculates finished auction's price for billing purposes.
   *
   * @param Drupal\auction\Entity\Auction $auction
   * @return int
   */
  public function determineFinishedAuctionPrice(Auction $auction) {
    $result = 0;
    if ($auction->hasBids()) {
      $result = $this->determineCurrentPrice($auction->getBids('standard'), $auction->getStartingPrice());
      $minimumPrice = $auction->getMinimumPrice();
      if ($result <= $minimumPrice) {
        $result = 0;
      }
    }
    return $result;
  }

  /**
   * Calculates current auction price.
   *
   * @param array $last_bids
   * @param int $starting_price
   * @return int
   */
  public function determineCurrentPrice($last_bids, $starting_price) {
    $result = $starting_price;

    if (count($last_bids) > 0) {
      // Get auction.
      $bid = reset($last_bids);
      $prev_bid = next($last_bids);
      $auction = $bid->getAuction();

      $result = $bid->amount / 100;

/*
      switch ($bid->type) {
        case Bid::TYPE_STANDARD:
          $result = $bid->amount / 100;
          break;

        case Bid::TYPE_INSTANT_BUY:
          $result = $bid->amount / 100;
          break;

        case Bid::TYPE_BUYING_ORDER:
          if (count($last_bids) == 1) {
            // Auction has only one bid.
            $result = $starting_price + $this->getBidStep($auction);
          }
          elseif ($prev_bid->amount != $bid->amount) {
            // Auction has more than one bid and 2 first bids aren't the same.
            $result = $prev_bid->amount / 100 + $this->getBidStep($auction);
          }
          else {
            // Both first bids have the same value.
            $result = $bid->amount / 100;
          }
          break;
      }
*/
    }

    return $result;
  }
}
