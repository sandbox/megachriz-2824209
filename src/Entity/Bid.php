<?php

namespace Drupal\auction\Entity;

use Drupal\auction\Auction as AuctionLib;
use \Entity;

/**
 * Entity class for a bid.
 */
class Bid extends Entity {
  // ---------------------------------------------------------------------------
  // CONSTANTS
  // ---------------------------------------------------------------------------

  // @todo bid types are now plugins...

  /**
   * Normal bid type, immediately visible to everyone.
   *
   * @var int
   */
  const TYPE_STANDARD  = 'standard';

  /**
   * @todo Document this one.
   *
   * @var int
   */
  const TYPE_INSTANT_BUY  = 'instant_buy';

  /**
   * The buying order type makes the real amount of the bid invisible.
   *
   * @var int
   */
  const TYPE_BUYING_ORDER  = 'buying_order';

  // ---------------------------------------------------------------------------
  // PROPERTIES
  // ---------------------------------------------------------------------------

  /**
   * The auction where the bid belongs to.
   *
   * @var \Drupal\auction\Entity\Auction
   */
  private $auction;

  // ---------------------------------------------------------------------------
  // CONSTRUCT
  // ---------------------------------------------------------------------------

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    if (!isset($this->status)) {
      $this->status = TRUE;
    }
  }

  /**
   * Magic wakeup.
   */
  public function __sleep() {
    // Save everything except auction.
    $keys = array_keys(get_object_vars($this));
    $key = array_search('auction', $keys);
    unset($keys[$key]);

    return $keys;
  }

  /**
   * Magic wakeup.
   */
  public function __wakeup() {
    // Reload attached auction (if any).
    $this->auction = NULL;
  }

  // ---------------------------------------------------------------------------
  // GETTERS
  // ---------------------------------------------------------------------------

  /**
   * Returns whether or not the bid is new.
   *
   * @return bool
   *   TRUE if the bid is new.
   *   FALSE otherwise.
   */
  public function isNew() {
    return !($this->identifier() > 0);
  }

  /**
   * Returns if this bid is currently the known highest in the auction.
   *
   * @return bool
   *   TRUE if the bid is the highest bid.
   *   FALSE otherwise.
   */
  public function isHighestBid() {
    $bid = $this->getAuction()->getHighestBid($this->type);
    if (!$bid || $bid->identifier() != $this->identifier()) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Returns the auction that the bid is attached to.
   *
   * @return \Drupal\auction\Entity\Auction
   *   An Auction instance.
   */
  public function getAuction() {
    if ($this->auction instanceof Auction) {
      return $this->auction;
    }

    $this->auction = entity_load_single('auction', $this->auction_id);
    return $this->auction;
  }

  /**
   * Returns handler for this bid type.
   */
  public function getHandler() {
    return AuctionLib::getBidTypePlugin($this->type, $this->getAuction());
  }

  /**
   * Display the price for this bid.
   *
   * @todo
   */
  public function displayPrice() {
    switch ($this->type) {
      case static::TYPE_BUYING_ORDER:
        $auction = $this->getAuction();
        if ($auction->current_price < $this->amount) {
          return AuctionLib::formatPrice($auction->getCurrentPrice(), $this->currency_code);
        }
        break;
    }

    return $this->formatPrice();
  }

  /**
   * Formats price.
   */
  public function formatPrice() {
    return AuctionLib::formatPrice($this->amount / 100, $this->currency_code);
  }

  /**
   * Implements Entity::defaultUri().
   */
  protected function defaultUri() {
    return array('path' => 'auction_bid/' . $this->identifier());
  }

  // ---------------------------------------------------------------------------
  // GETTERS
  // ---------------------------------------------------------------------------

  /**
   * Activates a bid.
   */
  public function activate() {
    if (!$this->status) {
      $this->status = TRUE;
      $this->save();
    }
    return $this;
  }

  /**
   * Deactivates a bid.
   */
  public function deactivate() {
    if ($this->status) {
      $this->status = FALSE;
      $this->save();
    }
    return $this;
  }
}
