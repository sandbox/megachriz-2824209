<?php

namespace Drupal\auction\Entity;

use \Entity;
use \EntityFieldQuery;
use Drupal\auction\Auction as AuctionLib;
use Drupal\auction\Exception\NotFoundException;

/**
 * Entity class for entity type 'auction'.
 */
class Auction extends Entity {
  // ---------------------------------------------------------------------------
  // CONSTANTS
  // ---------------------------------------------------------------------------

  /**
   * The auction has yet to begin.
   *
   * @var int
   */
  const STATUS_NOT_YET_STARTED = 1;

  /**
   * The auction is active.
   *
   * @var int
   */
  const STATUS_ACTIVE = 2;

  /**
   * The auction has finished.
   *
   * @var int
   */
  const STATUS_FINISHED = 3;

  // ---------------------------------------------------------------------------
  // GETTERS
  // ---------------------------------------------------------------------------

  /**
   * Returns bids.
   *
   * @param int $type
   *   (optional) The bid type to filter on.
   * @param array $options
   *   (optional) The following options can be set:
   *   - include_inactive
   *     If true, inactive bids will also be searched.
   *     Defaults to false.
   *
   * @return array
   *   A list of bids.
   */
  public function getBids($type = NULL, array $options = array()) {
    $options += array(
      'include_inactive' => FALSE,
    );

    if ($type || !$options['include_inactive']) {
      return array_filter($this->bids, function($bid) use ($type, $options) {
        if ($type && $bid->type != $type) {
          return FALSE;
        }
        if (!$options['include_inactive'] && !$bid->status) {
          return FALSE;
        }
        return TRUE;
      });
    }

    return $this->bids;
  }

  /**
   * Iterates through bids, then returns the specified bid in the loop.
   *
   * @param int $index
   *   The number of bid to return.
   * @param int $type
   *   (optional) The bid type to filter on.
   * @param array $options
   *   (optional) See ::getBids().
   *
   * @return \Drupal\auction\Entity\Bid|null
   *   A bid instance, if found.
   *   NULL otherwise.
   */
  public function getBidAtIndex($index, $type = NULL, array $options = array()) {
    if ($index === 0) {
      return $this->getLastBid();
    }

    $bids = $this->getBids($type, $options);
    reset($bids);
    for ($i = 0; $i < $index; $i++) {
      next($bids);
    }

    return current($bids);
  }

  /**
   * Returns last bid.
   *
   * @return \Drupal\auction\Entity\Bid|null
   *   A bid instance, if found.
   *   NULL otherwise.
   */
  public function getLastBid() {
    return reset($this->bids);
  }

  /**
   * Returns highest bid placed in auction.
   *
   * @param string $type
   *   (optional) The bid type to filter on.
   *   Defaults to Drupal\auction\Entity\Bid::TYPE_STANDARD.
   * @param array $options
   *   (optional) See ::getBids().
   *
   * @return stdClass|null
   *   A bid object, if found.
   *   NULL otherwise.
   */
  public function getHighestBid($type = Bid::TYPE_STANDARD, array $options = array()) {
    if ($this->hasBids()) {
      $bids = $this->getBids($type, $options);
      return reset($bids);
    }
  }

  /**
   * Returns the previous highest bid.
   *
   * @param string $type
   *   (optional) The bid type to filter on.
   *   Defaults to Drupal\auction\Entity\Bid::TYPE_STANDARD.
   * @param array $options
   *   (optional) See ::getBids().
   *
   * @return stdClass|null
   *   A bid object, if found.
   *   NULL otherwise.
   */
  public function getPreviousHighestBid($type = Bid::TYPE_STANDARD, array $options = array()) {
    return $this->getBidAtIndex(1, $type, $options);
  }

  /**
   * Returns amount of highest bid placed in auction.
   *
   * @param string $type
   *   (optional) The bid type to filter on.
   *   Defaults to Drupal\auction\Entity\Bid::TYPE_STANDARD.
   * @param array $options
   *   (optional) See ::getBids().
   *
   * @return int
   *   A bid amount, if found.
   *   NULL otherwise.
   */
  public function getHighestBidAmount($type = Bid::TYPE_STANDARD, array $options = array()) {
    $bid = $this->getHighestBid($type, $options);
    if ($bid) {
      return $bid->amount / 100;
    }
  }

  /**
   * Returns whether or not the auction has bids.
   *
   * @param string $type
   *   (optional) The bid type to filter on.
   *   Defaults to Drupal\auction\Entity\Bid::TYPE_STANDARD.
   *
   * @return bool
   *   True if the auction has bids.
   *   False otherwise.
   */
  public function hasBids($type = Bid::TYPE_STANDARD) {
    return count($this->getBids($type));
  }

  /**
   * Returns uid of the user currenly winning in auction.
   *
   * @return int
   *   The ID of the user that is currently winning.
   */
  public function getWinningUserId() {
    $bid = $this->getHighestBid();
    if ($bid) {
      return $bid->uid;
    }
  }

  /**
   * Returns name of the user currenly winning in auction.
   *
   * @return string
   *   The name of the user that is currently winning.
   *
   * @todo remove?
   */
  public function getWinningUserName() {
    return db_select('users', 'u')
      ->fields('u', array('name'))
      ->condition('uid', $this->getWinningUserId())
      ->execute()
      ->fetchField();
  }

  /**
   * Returns true if user is winning in auction or false otherwise.
   *
   * @param int uid
   *   The user ID to check for.
   *
   * @return bool
   *   TRUE if the given user is currently winning.
   *   FALSE otherwise.
   */
  public function userIsWinning($uid) {
    return $uid == $this->getWinningUserId();
  }

  /**
   * Returns node that references target auction.
   *
   * @return object|null
   *   A node object if found.
   *   Null otherwise.
   */
  public function getParentNode() {
    $query = new EntityFieldQuery();

    $query->entityCondition('entity_type', 'node')
      ->fieldCondition('field_auction', 'target_id', $this->auction_id, '=')
      ->entityOrderBy('entity_id', 'DESC')
      ->addMetaData('account', user_load($this->uid));

    $result = $query->execute();

    if (isset($result['node']) && !empty($result['node'])) {
      $nids = array_keys($result['node']);
      $nid = array_shift($nids);
      return node_load($nid);
    }
  }

  /**
   * Returns starting price of an auction.
   *
   * @return int
   *   The starting price, in whole units.
   */
  public function getStartingPrice() {
    return $this->getWrapper()->field_auction_starting_price->value();
  }

  /**
   * Returns minimum price of an auction.
   *
   * @return int
   *   The starting price, in cents.
   */
  public function getMinimumPrice() {
    return $this->getWrapper()->field_auction_minimum_price->value();
  }

  /**
   * Returns current price of an auction.
   *
   * @return int
   *   The current price, in whole units.
   */
  public function getCurrentPrice() {
    return $this->current_price / 100;
  }

  /**
   * Returns the auction status.
   *
   * @return int
   *   The status of the auction, see defined constants in Auction class.
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * Returns whether or not the auction is active.
   *
   * @return bool
   *   True if the auction is active.
   *   False otherwise.
   */
  public function isActive() {
    return $this->status == static::STATUS_ACTIVE;
  }

  /**
   * Returns whether or not the auction has finished.
   *
   * @return bool
   *   True if the auction has finished.
   *   False otherwise.
   */
  public function hasFinished() {
    return $this->status == static::STATUS_FINISHED;
  }

  /**
   * Returns whether or not the auction has expired.
   *
   * @return bool
   *   True if the auction has expired.
   *   False otherwise.
   */
  public function hasExpired() {
    return $this->field_auction_date[LANGUAGE_NONE][0]['value2'] <= REQUEST_TIME || $this->hasFinished();
  }

  /**
   * Defines the entity label if the 'entity_class_label' callback is used.
   *
   * Specify 'entity_class_label' as 'label callback' in hook_entity_info() to
   * let the entity label point to this method. Override this in order to
   * implement a custom default label.
   */
  protected function defaultLabel() {
    return t('Auction @auction_id', array(
      '@auction_id' => $this->identifier(),
    ));
  }

  /**
   * Implements Entity::defaultUri().
   */
  protected function defaultUri() {
    // It works only for nodes.
    return array('path' => 'node/' . $this->getParentNode()->nid);
  }

  /**
   * Returns wrapper for given auction.
   *
   * @return EntityMetadataWrapper
   *   A metadata wrapper for the auction.
   */
  public function getWrapper() {
    return entity_metadata_wrapper('auction', $this);
  }

  // ---------------------------------------------------------------------------
  // ACTION
  // ---------------------------------------------------------------------------

  /**
   * Activates an auction.
   */
  public function activate() {
    if ($this->isActive()) {
      // Auction is already active.
      return $this;
    }

    $auction_node = $this->getParentNode();
    if (!$auction_node) {
      throw new NotFoundException('No auction node found for auction ' . $this->auction_id);
    }
    rules_invoke_all('auction_started', $this, $auction_node);

    $this->status = static::STATUS_ACTIVE;
    $this->save();

    return $this;
  }

  /**
   * Deactivates an auction.
   */
  public function deactivate() {
    if ($this->hasFinished()) {
      // Auction has already finished.
      return $this;
    }

    $auction_node = $this->getParentNode();
    if (!$auction_node) {
      throw new NotFoundException('No auction node found for auction ' . $this->auction_id);
    }
    rules_invoke_all('auction_finished', $this, $auction_node);

    $this->status = static::STATUS_FINISHED;
    $this->save();

    return $this;
  }

  /**
   * Formats a price.
   *
   * @param int $amount
   *   The amount.
   *
   * @return string
   *   The formatted price.
   */
  public function formatPrice($amount) {
    return AuctionLib::formatPrice($amount, $this->currency_code);
  }

  /**
   * Formats a price.
   *
   * @param int $amount_in_cents
   *   The amount in cents.
   *
   * @return string
   *   The formatted price.
   */
  public function formatPriceInCents($amount) {
    return $this->formatPrice($amount / 100);
  }
}
