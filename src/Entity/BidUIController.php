<?php

namespace Drupal\auction\Entity;

use \EntityOperationsDefaultAdminUIController;

/**
 * Entity UI controller for entity type "auction_bid".
 */
class BidUIController extends EntityOperationsDefaultAdminUIController {
  /**
   * {@inheritdoc}
   */
  public function hook_menu() {
    $items = parent::hook_menu();

    $admin_path = 'admin/content/auction_bids';
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%entity_object';

    $adjust = array(
      '',
      '/list',
    );
    foreach ($adjust as $subpath) {
      $items[$admin_path . $subpath] = $items[$this->path . $subpath];
      unset($items[$this->path . $subpath]);
    }

    // Remove all manage from links.
    foreach ($items as $path => $item) {
      if (strpos($path, '/manage/')) {
        $new_path = str_replace('/manage/', '/', $path);
        $items[$new_path] = $items[$path];
        if (isset($items[$new_path]['title arguments'][1])) {
          $items[$new_path]['title arguments'][1]--;
        }
        if (isset($items[$new_path]['page arguments'][1])) {
          $items[$new_path]['page arguments'][1]--;
        }
        if (isset($items[$new_path]['access arguments'][2])) {
          $items[$new_path]['access arguments'][2]--;
        }
        unset($items[$path]);
      }
    }

    // Add in back general entity uri.
    $items[$this->path . '/' . $wildcard] = array(
      'title' => 'Edit',
      'title callback' => 'entity_label',
      'title arguments' => array($this->entityType, $this->id_count),
      'page callback' => 'entity_ui_get_form',
      'page arguments' => array($this->entityType, $this->id_count),
      'load arguments' => array($this->entityType),
      'access callback' => 'entity_access',
      'access arguments' => array('update', $this->entityType, $this->id_count),
    );

    return $items;
  }
}
