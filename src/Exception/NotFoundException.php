<?php

namespace Drupal\auction\Exception;

use \Exception;

/**
 * Thrown if a requested entity could not be found.
 */
class NotFoundException extends Exception {}
