<?php

namespace Drupal\auction\Exception;

use \Exception;

/**
 * Thrown if an user tries to place an invalid bid.
 */
class InvalidBidException extends Exception {}
