<?php

namespace Drupal\auction\Plugin\auction\BidType;

/**
 * Interface for bid types with a bid amount element.
 */
interface BidAmountInterface {
  /**
   * Verifies bid amount.
   */
  public function bidAmountValidate(&$element, &$form_state, &$form);
}
