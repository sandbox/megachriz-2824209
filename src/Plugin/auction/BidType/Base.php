<?php

namespace Drupal\auction\Plugin\auction\BidType;

use Drupal\auction\Entity\Auction;
use Drupal\auction\Entity\Bid;

/**
 * Base class for types of bids.
 */
abstract class Base implements BidTypeInterface {
  /**
   * The bid type identifier.
   *
   * @var string
   */
  protected $type;

  /**
   * The auction used for bids.
   *
   * @var \Drupal\auction\Entity\Auction
   */
  protected $auction;

  /**
   * Object constructor.
   *
   * @param \Drupal\auction\Entity\Auction $auction
   *   The auction used for bids.
   */
  public function __construct($type, Auction $auction) {
    $this->type = $type;
    $this->auction = $auction;
  }

  /**
   * Returns the bid type identifier.
   *
   * @return string
   *   The bid type identifier.
   */
  public function identifier() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function bidFormValidate(&$form, &$form_state) {}

  /**
   * {@inheritdoc}
   */
  public function bidFormSubmit(&$form, &$form_state) {}

  /**
   * {@inheritdoc}
   */
  public function onSaveBid(Bid $bid, $is_new) {}
}
