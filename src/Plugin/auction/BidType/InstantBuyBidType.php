<?php

namespace Drupal\auction\Plugin\auction\BidType;

use Drupal\auction\Auction as AuctionLib;
use Drupal\auction\Entity\Auction;

/**
 * Class for instant buy.
 *
 * @todo untested.
 */
class InstantBuyBidType extends Base {
  /**
   * {@inheritdoc}
   */
  public function isActive() {
    // Instant buy is only available in auctions with price set.
    $instantBuyPrice = $this->getPrice();
    if (!$instantBuyPrice) {
      return FALSE;
    }

    $settings = AuctionLib::getSettings();
    // Disable buy now if first bid was placed.
    if ($settings['buy_now_mode'] == 'first bid') {
      AuctionLib::getController()->attachBids($this->auction);

      if (empty($auction->bids)) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    // Disable buy now when price reaches limit.
    if ($settings['buy_now_mode'] == 'percentage') {
      $currentPrice = $this->auction->getHighestBidAmount();
      $threshold = $instantBuyPrice / 100 * $settings['buy_now_threshold'] / 100;

      if ($currentPrice < $threshold) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function bidForm(&$form, &$form_state) {
    $form_state['auction']['instant_buy_price'] = $this->getPrice();

    $form['price'] = array(
      '#type' => 'item',
      '#title' => t('Instant buy price'),
      '#markup' => AuctionLib::formatPrice($this->getPrice(), $this->auction->currency_code),
    );
    $form['actions']['instant_buy_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Buy now'),
      '#bid_type' => $this->identifier(),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function bidFormValidate(&$form, &$form_state) {
    global $user;
    $controller = AuctionLib::getController();

    // We load auction again to make sure it is active.
    $auction = entity_load_single('auction', $form_state['auction']['auction_id']);
    $auction = $controller->changeStatusIfNeeded($auction);

    // @todo Needs update. Created bid is not winning bid.
    if ($auction->status == Auction::STATUS_ACTIVE) {
      $bid = entity_create('auction_bid', array(
        'auction_id' => $form_state['auction']['auction_id'],
        'amount' => $form_state['auction']['instant_buy_price'],
        'currency_code' => $form_state['auction']['currency'],
        'uid' => $user->uid,
        'type' => $this->identifier(),
        'source' => $this->identifier(),
      ));
      $controller->saveBid($bid);
      $auction->deactivate();
    }
    else {
      form_set_error('', t('Auction has ended.'));
    }
  }

  /**
   * Returns price for which auction can be instant bought.
   *
   * @return float
   */
  public function getPrice() {
    return $this->auction->getWrapper()->field_auction_buy_now_price->value();
  }
}
