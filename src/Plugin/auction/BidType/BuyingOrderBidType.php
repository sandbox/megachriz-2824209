<?php

namespace Drupal\auction\Plugin\auction\BidType;

use \Exception;
use \InvalidArgumentException;
use Drupal\auction\Auction as AuctionLib;
use Drupal\auction\Entity\Auction;
use Drupal\auction\Entity\Bid;
use Drupal\auction\Exception\InvalidBidException;

/**
 * Class for buying orders.
 */
class BuyingOrderBidType extends StandardBidType implements BidAmountInterface {
  /**
   * {@inheritdoc}
   */
  public function bidForm(&$form, &$form_state) {
    global $user;

    $buying_order = $this->auction->getHighestBid($this->identifier());
    if ($buying_order && $buying_order->uid == $user->uid) {
      $form['your_buying_order'] = array(
        'amount' => array(
          '#type' => 'item',
          '#title' => t('Your buying order'),
          '#markup' => $buying_order->formatPrice(),
        ),
        'remove' => array(
          '#theme' => 'link',
          '#text' => t('Remove buying order'),
          '#path' => 'auction/' . $this->auction->identifier() . '/remove-buying-order/' . $buying_order->identifier(),
          '#options' => array(
            'attributes' => array(),
            'html' => FALSE,
            'query' => drupal_get_destination(),
          ),
          '#access' => auction_bids_remove_buying_order_access($this->auction, $buying_order->identifier()),
        ),
      );
    }

    $form['actions']['buying_order'] = array(
      '#type' => 'submit',
      '#value' => t('Place buying order'),
      '#bid_type' => $this->identifier(),
    );
  }

  /**
   * Form callback for confirming a buying order.
   */
  public function confirmBidForm(&$form, &$form_state) {
    $bid = $form_state['bid'];

    $variables = array(
      '!amount' => '<span class="amount">' . $bid->formatPrice() . '</span>',
      '@auction' => $bid->getAuction()->getParentNode()->title,
    );

    $question = t('Bidding on @auction', $variables);
    $description = t('Are you sure you want to place a buying order of !amount on @auction?', $variables);
    $path = current_path();

    $form = confirm_form($form, $question, $path, $description, NULL, NULL, 'confirm_' . $bid->getAuction()->identifier());

    $form['bid_amount'] = array(
      '#type' => 'value',
      '#element_validate' => array('auction_bids_bid_amount_validate'),
      '#value' => $bid->amount,
    );

    return $form;
  }

  /**
   * Submit handler for confirming a buying order.
   */
  public function confirmBidFormSubmit(&$form, &$form_state) {
    $buying_order = $form_state['bid'];
    $success = $this->placeBuyingOrder($buying_order);
    if ($success && $this->checkHighestBid($buying_order, $this->auction)) {
      drupal_set_message(t('Your buying order has been placed.'));
    }
  }

  /**
   * Places the given buying order.
   *
   * @param \Drupal\auction\Entity\Bid $buying_order
   *   The buying order to place.
   *
   * @throws \InvalidArgumentException
   *   In case the given bid is not a buying order.
   */
  public function placeBuyingOrder(Bid $buying_order) {
    // Reasons to skip out early.
    if ($buying_order->type != 'buying_order') {
      throw new InvalidArgumentException('Given bid is not of type buying_order.');
    }

    // Start a new transaction.
    $transaction = db_transaction();
    try {
      $controller = AuctionLib::getController();

      // Reload auction.
      $this->auction = entity_load_single('auction', $buying_order->auction_id, TRUE);

      // Check if the buying order's amount is higher than the current highest bid.
      $highest_bid = $this->auction->getHighestBid('standard');
      if ($highest_bid && $highest_bid->amount >= $buying_order->amount && $highest_bid->uid != $buying_order->uid) {
        // The user has been outbid. Do not place buying order, but save a normal
        // bid instead.
        $buying_order->type = 'standard';
        return $this->placeBid($buying_order);
      }

      // Check if there is an other buying order.
      $highest_buying_order = $this->auction->getHighestBid($this->identifier());
      if ($highest_buying_order instanceof Bid && $highest_buying_order->uid != $buying_order->uid) {
        // The other buying order belongs to an other user. Create bids in
        // between.
        $this->createInBetweenBids($highest_buying_order, $buying_order);
      }

      // Reload bids on auction again.
      $controller->attachBids($this->auction);

      // Create a normal bid, but only if bid from highest bidder was from someone
      // else.
      $highest_bid = $this->auction->getHighestBid('standard');
      if (!$highest_bid || $highest_bid->uid != $buying_order->uid) {
        $this->createAutoBid($buying_order, $this->identifier() . '_new', FALSE);
      }

      // Check if the highest buying order belongs to the same user.
      if ($highest_buying_order instanceof Bid && $highest_buying_order->uid == $buying_order->uid) {
        // The highest buying order belongs to the current user, only adjust the
        // amount of the buying order.
        $highest_buying_order->amount = $buying_order->amount;
        return $controller->saveBid($highest_buying_order);
      }

      // Finally, save the buying order.
      $buying_order->save();
      $controller->attachBids($this->auction);

      // Notify about saved buying order, but only if it is still a buying order
      // (and hasn't been converted to a normal bid).
      if ($buying_order->type == $this->identifier()) {
        $controller->notifyPlacedBid($this->auction, $buying_order);
      }

      // Notify also about a new bid.
      $controller->notifyPlacedBid($this->auction);

      return TRUE;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('auction', $e);
      return FALSE;
    }
  }

  /**
   * A new bid has been saved.
   *
   * @param \Drupal\auction\Entity\Bid $bid
   *   The bid that was saved.
   * @param bool $is_new
   *   If the bid is a new bid.
   */
  public function onSaveBid(Bid $bid, $is_new) {
    if ($is_new && $bid->type == 'standard') {
      $highest_bid = $this->auction->getHighestBid('standard');
      $buying_order = $this->auction->getHighestBid($this->identifier());

      if ($buying_order) {
        // Check if an automatic bid should be created. This should happen in the following
        // circumstances:
        // - The saved bid is *not* from the user with the highest buying order.
        $a = ($buying_order->uid != $bid->uid);
        // - The user with the highest buying order does *not* have the highest bid already.
        $b = ($buying_order->uid != $highest_bid->uid);
        // - The buying order amount is higher than or equal to that from the saved bid.
        $c = ($buying_order->amount >= $bid->amount);

        if ($a && $b && $c) {
          $this->createAutoBid($buying_order);
        }
      }
    }

    // Deactivate all buying orders that are below the current auction price.
    foreach ($this->auction->getBids($this->identifier()) as $buying_order) {
      if ($buying_order->amount <= $this->auction->current_price) {
        $buying_order->deactivate();
      }
    }
  }

  /**
   * Creates a new bid.
   *
   * @param \Drupal\auction\Entity\Bid $buying_order
   *   The buying order to create a bid for.
   * @param string $source
   *   (optional) The source of bidding.
   *   Defaults to identifier of this bid type plugin.
   * @param bool $notify
   *   (optional) If the autobid should result into a notification.
   *   Defaults to TRUE.
   *
   * @todo Needs cleanup.
   */
  protected function createAutoBid(Bid $buying_order, $source = NULL, $notify = TRUE) {
    $controller = AuctionLib::getController();

    // Determine bid amount, but take possible other buying orders in account.
    $bid_amount = $controller->getMinimumBidAmount($this->auction);
    $current_amount = NULL;
    $highest_bid = $this->auction->getHighestBid(NULL);
    if ($highest_bid) {
      if ($highest_bid->uid == $buying_order->uid) {
        // Highest bid is from current user. Loop through all bids until we find
        // one that does *not* belong to the buying order user.
        foreach ($this->auction->getBids() as $bid) {
          if ($bid->uid != $buying_order->uid) {
            // Found a bid belonging to someone else. Now break out of the loop.
            $current_amount = $bid->amount / 100;
            break;
          }
        }
      }
      else {
        $current_amount = $highest_bid->amount / 100;
      }
    }

    if ($bid_amount <= $current_amount) {
      $bid_amount = $current_amount + $controller->getBidStep($this->auction);
    }

    // Create a new bid.
    $normal_bid = $this->createBidBasedOnBuyingOrder($buying_order, $bid_amount * 100);
    $normal_bid->source = is_null($source) ? $this->identifier() : $source;
    if ($normal_bid->amount > $buying_order->amount) {
      // If the normal bid would exceed the buying order, convert the buying
      // order to a normal bid.
      // @todo DON'T?
      $buying_order->type = 'standard';
      $buying_order->created = REQUEST_TIME;
      $controller->saveBid($buying_order, $notify);
      return;
    }
    $controller->saveBid($normal_bid, $notify);
  }

  /**
   * Creates bids that should exist between bid a and bid b.
   *
   * @param \Drupal\auction\Entity\Bid $buying_order_a
   *   The first buying order.
   * @param \Drupal\auction\Entity\Bid $buying_order_b
   *   The second buying order.
   *
   * @throws \InvalidArgumentException
   *   In case one of the given bids is not a buying order.
   */
  protected function createInBetweenBids(Bid $buying_order_a, Bid $buying_order_b) {
    // Reasons to skip out early.
    if ($buying_order_a->uid == $buying_order_b->uid) {
      // Both buying orders are owned by the same person. Do nothing.
      return;
    }
    if ($buying_order_a->type != 'buying_order') {
      throw new InvalidArgumentException('Given first bid is not of type buying_order.');
    }
    if ($buying_order_b->type != 'buying_order') {
      throw new InvalidArgumentException('Given second bid is not of type buying_order.');
    }

    $controller = AuctionLib::getController();

    // Sort out which of the buying orders is the lowest one.
    $bids = array($buying_order_a, $buying_order_b);
    uasort($bids, array(get_class($this), 'bidSort'));
    list($lowest_buying_order, $highest_buying_order) = array_values($bids);

    // Determine first bid amount and with who to start to place a sandwich bid.
    $current_highest_bid = $this->auction->getHighestBid('standard');
    $new_bid_amount = $current_highest_bid->amount + ($controller->getBidStep($this->auction) * 100);
    $current_buying_order = ($current_highest_bid->uid == $highest_buying_order->uid) ? $lowest_buying_order : $highest_buying_order;

    while ($new_bid_amount <= $lowest_buying_order->amount) {
      $sandwich_bid = $this->createBidBasedOnBuyingOrder($current_buying_order, $new_bid_amount);
      $sandwich_bid->save();
      $controller->updateCurrentPrice($this->auction);

      $new_bid_amount += ($controller->getBidStep($this->auction) * 100);
      $current_buying_order = ($current_buying_order === $highest_buying_order) ? $lowest_buying_order : $highest_buying_order;
    }

    // If the last bid was from the highest bidder, then a bid from the lowest
    // bidder should be created to match with his/her maximum bid.
    if ($sandwich_bid->uid == $highest_buying_order->uid) {
      // Check which bid should be displayed first.
      if ($sandwich_bid->amount != $highest_buying_order->amount) {
        // The bid from the lowest buying order should be created first, so
        // switch bid owners.
        $sandwich_bid->uid = $lowest_buying_order->uid;
        $sandwich_bid->save();
        // And create a bid for the highest buying order on top.
        $bid = $this->createBidBasedOnBuyingOrder($highest_buying_order, $sandwich_bid->amount);
        $bid->save();
      }
      else {
        // Just create a bid for the lowest bidder.
        $bid = $this->createBidBasedOnBuyingOrder($lowest_buying_order, $sandwich_bid->amount);
        $bid->save();
      }

      // Create a new bid for the highest bidder if it is still below the amount
      // of the highest buying order.
      if ($sandwich_bid->amount < $highest_buying_order->amount) {
        $amount = $sandwich_bid->amount += ($controller->getBidStep($this->auction) * 100);
        $bid = $this->createBidBasedOnBuyingOrder($highest_buying_order, $sandwich_bid->amount);

        // If it now exceeds the amount of the highest buying order, make it
        // equal to buying order amount.
        if ($bid->amount > $highest_buying_order->amount) {
          $bid->amount = $highest_buying_order->amount;
        }

        $bid->save();
      }
    }
    // If the last bid was from the lowest bidder, then a bid from the highest
    // bidder should be created on top.
    else {
      // Check if the amounts of buying orders are equal.
      if ($lowest_buying_order->amount == $highest_buying_order->amount) {
        // Because the bid belonging to the user who placed the highest buying
        // order should get priority, we need to change the owner of the created
        // last bid and create another one for the lowest bidder.
        $sandwich_bid->uid = $highest_buying_order->uid;
        $sandwich_bid->save();

        $bid = $this->createBidBasedOnBuyingOrder($lowest_buying_order, $lowest_buying_order->amount);
        $bid->save();
      }
      else {
        // A new bid should placed for the highest buying order.
        $amount = $lowest_buying_order->amount + ($controller->getBidStep($this->auction) * 100);
        $bid = $this->createBidBasedOnBuyingOrder($highest_buying_order, $amount);

        // If it now exceeds the amount of the highest buying order, make it
        // equal to buying order amount.
        if ($bid->amount > $highest_buying_order->amount) {
          $bid->amount = $highest_buying_order->amount;
        }
        $bid->save();
      }
    }

    // Deactivate the lowest buying order.
    $lowest_buying_order->deactivate();

    // If the amount of the highest buying order is the same, deactivate that
    // one as well.
    if ($lowest_buying_order->amount == $highest_buying_order->amount) {
      $highest_buying_order->deactivate();
    }
  }

  /**
   * Creates a new bid based on given buying order.
   *
   * @param \Drupal\auction\Entity\Bid
   *   The buying order.
   * @param int $amount
   *   The amount that the new bid should get.
   *
   * @return \Drupal\auction\Entity\Bid
   *   The created bid.
   */
  protected function createBidBasedOnBuyingOrder(Bid $buying_order, $amount) {
    return entity_create('auction_bid', array(
      'type' => 'standard',
      'auction_id' => $buying_order->auction_id,
      'amount' => $amount,
      'currency_code' => $buying_order->currency_code,
      'uid' => $buying_order->uid,
      'source' => $this->identifier(),
    ));
  }

  /**
   * Callback for sorting two bids.
   *
   * @param \Drupal\auction\Entity\Bid $bid_a
   *   The first bid.
   * @param \Drupal\auction\Entity\Bid $bid_b
   *   The second bid.
   * @param array $options
   *   (optional) Which comparisons should be made:
   *   - amount
   *   - created
   *   Defaults to checking all. Checking for bid_id cannot be turned off.
   */
  public static function bidSort(Bid $bid_a, Bid $bid_b, $options = array()) {
    $options += array(
      'amount' => TRUE,
      'created' => TRUE,
    );

    // First check amount (ASC). The lowest bid should come first.
    if ($options['amount']) {
      $result = static::bidSortHelper($bid_a->amount, $bid_b->amount);
      if ($result != 0) {
        return $result;
      }
    }

    // If amounts are the same, check the created time (DESC). The later placed
    // bid is 'lower'.
    if ($options['created']) {
      $created_a = isset($bid_a->created) ? $bid_a->created : REQUEST_TIME;
      $created_b = isset($bid_b->created) ? $bid_b->created : REQUEST_TIME;
      $result = static::bidSortHelper($created_b, $created_a);
      if ($result != 0) {
        return $result;
      }
    }

    // If created at the same time, check bid ID (DESC). The last placed bid is
    // 'lower'. We reverse the ID's here, as a new bid (0) should always come
    // last.
    $id_a = $bid_a->isNew() ? 0 : 0 - $bid_a->identifier();
    $id_b = $bid_b->isNew() ? 0 : 0 - $bid_b->identifier();
    return static::bidSortHelper($id_a, $id_b);
  }

  /**
   * Helper function for sorting bids.
   *
   * @param int $a
   *   The first value.
   * @param int $b
   *   The second value.
   */
  protected static function bidSortHelper($a, $b) {
    if ($a == $b) {
      return 0;
    }
    return ($a < $b) ? -1 : 1;
  }

}
