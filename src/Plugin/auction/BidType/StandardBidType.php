<?php

namespace Drupal\auction\Plugin\auction\BidType;

use Drupal\auction\Auction as AuctionLib;
use Drupal\auction\Entity\Auction;
use Drupal\auction\Entity\Bid;
use Drupal\auction\Exception\InvalidBidException;

/**
 * Standard type for bids.
 */
class StandardBidType extends Base implements BidAmountInterface {
  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return TRUE;
    //return $auction->isBiddingActive();
  }

  /**
   * {@inheritdoc}
   */
  public function bidForm(&$form, &$form_state) {
    $form['actions']['standard'] = array(
      '#type' => 'submit',
      '#value' => t('Place bid'),
      '#bid_type' => $this->identifier(),
    );
  }

  /**
   * Verifies bid amount.
   */
  public function bidAmountValidate(&$element, &$form_state, &$form) {
    $controller = AuctionLib::getController();

    // We load auction again to make sure it's active.
    $auction = entity_load_single('auction', $form_state['auction']['auction_id']);
    $auction = $controller->changeStatusIfNeeded($auction);

    if (!$auction->isActive()) {
      form_set_error('', t('Auction has ended.'));
    }
    else {
      try {
        $controller->validateBid($auction, $element['#value']);
      }
      catch (InvalidBidException $e) {
        form_error($element, $e->getMessage());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function bidFormSubmit(&$form, &$form_state) {
    global $user;

    $form_state['bid'] = entity_create('auction_bid', array(
      'auction_id' => $form_state['auction']['auction_id'],
      'amount' => $form_state['values']['bid_amount'] * 100,
      'currency_code' => $form_state['auction']['currency'],
      'uid' => $user->uid,
      'type' => $this->identifier(),
      'source' => $this->identifier(),
    ));

    $form_state['rebuild'] = TRUE;
  }

  /**
   * Form callback for confirming a bid.
   */
  public function confirmBidForm(&$form, &$form_state) {
    $bid = $form_state['bid'];

    $variables = array(
      '!amount' => '<span class="amount">' . $bid->formatPrice() . '</span>',
      '@auction' => $bid->getAuction()->getParentNode()->title,
    );

    $question = t('Bidding on @auction', $variables);
    $description = t('Are you sure you want to bid !amount on @auction?', $variables);
    $path = current_path();

    // Check if the bidder is already the highest bidder.
    if ($bid->getAuction()->getWinningUserId() == $bid->uid) {
      drupal_set_message(t('You are already the highest bidder.'), 'warning', FALSE);
      $description = t('You are already the highest bidder.') . ' ' . $description;
    }

    $form = confirm_form($form, $question, $path, $description, NULL, NULL, 'confirm_' . $bid->getAuction()->identifier());

    $form['bid_amount'] = array(
      '#type' => 'value',
      '#element_validate' => array('auction_bids_bid_amount_validate'),
      '#value' => $bid->amount,
    );

    return $form;
  }

  /**
   * Submit handler for confirming a bid.
   */
  public function confirmBidFormSubmit(&$form, &$form_state) {
    $auction = entity_load_single('auction', $form_state['auction']['auction_id']);
    $bid = $form_state['bid'];

    $success = $this->placeBid($bid);

    if ($success && $this->checkHighestBid($bid, $auction)) {
      drupal_set_message(t('Your bid has been placed.'));
    }
  }

  /**
   * Places the given bid.
   *
   * @param \Drupal\auction\Entity\Bid $bid
   *   The bid to place.
   *
   * @return bool
   *   True if placing bid was successful.
   *   False otherwise.
   */
  public function placeBid(Bid $bid) {
    return AuctionLib::getController()->saveBid($bid);
  }

  /**
   * Check if the user of the new bid is the highest bidder.
   *
   * @param \Drupal\auction\Entity\Bid
   *   The new placed bid.
   * @param \Drupal\auction\Entity\Auction
   *   The auction in which the bid is placed.
   *
   * @return bool
   *   True if the bidder of the new bid is the highest bidder.
   *   False otherwise.
   */
  protected function checkHighestBid(Bid $new_bid, Auction $auction) {
    $highest_bid = $auction->getHighestBid();

    if ($highest_bid->uid != $new_bid->uid) {
      // Person has been outbid.
      if ($highest_bid->amount == $new_bid->amount) {
        drupal_set_message(t(
          'You have been outbid by @username, because this user bet the same amount first.',
          array('@username' => AuctionLib::maskUsername($auction->getWinningUserName()))
        ), 'warning');
      }
      elseif ($highest_bid->amount > $new_bid->amount) {
        drupal_set_message(t(
          'You have been outbid by @username.',
          array('@username' => AuctionLib::maskUsername($auction->getWinningUserName()))
        ), 'warning');
      }

      return FALSE;
    }

    return TRUE;
  }
}
