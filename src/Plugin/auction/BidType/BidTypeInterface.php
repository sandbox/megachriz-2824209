<?php

namespace Drupal\auction\Plugin\auction\BidType;

use Drupal\auction\Entity\Bid;

/**
 * Interface for bid types.
 */
interface BidTypeInterface {
  /**
   * Returns if the plugin is active for the auction.
   *
   * @return bool
   *   TRUE if the plugin is active.
   *   FALSE otherwise.
   */
  public function isActive();

  /**
   * Form alter function for the bid form.
   */
  public function bidForm(&$form, &$form_state);

  /**
   * Validate handler for the bid form.
   */
  public function bidFormValidate(&$form, &$form_state);

  /**
   * Submit handler for the bid form.
   */
  public function bidFormSubmit(&$form, &$form_state);

  /**
   * Called when a bid gets saved.
   *
   * @param Drupal\auction\Entity\Bid $bid
   *   The bid that was saved.
   * @param bool $is_new
   *   If the bid is a new bid or an existing one.
   */
  public function onSaveBid(Bid $bid, $is_new);
}
