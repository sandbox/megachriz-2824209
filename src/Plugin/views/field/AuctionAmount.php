<?php

namespace Drupal\auction\Plugin\views\field;

use Drupal\auction\Auction;
use Drupal\auction\Entity\Bid;
use \views_handler_field;

/**
 * For displaying the bid amount, eventually with currency code.
 */
class AuctionAmount extends views_handler_field {

  function construct() {
    parent::construct();
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['show_currency'] = array(
      'default' => TRUE,
      'bool' => TRUE,
    );
    $options['plain'] = array(
      'default' => FALSE,
      'bool' => TRUE,
    );
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['show_currency'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show currency suffix'),
      '#default_value' => $this->options['show_currency'],
      '#states' => array(
        'disabled' => array(
          ':input[name="options[plain]"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['plain'] = array(
      '#type' => 'checkbox',
      '#title' => t('Unformatted'),
      '#default_value' => $this->options['plain'],
    );
    parent::options_form($form, $form_state);

  }

  function options_submit(&$form, &$form_state) {
    $this->options['show_currency'] = $form_state['values']['show_currency'];
    $this->options['plain'] = $form_state['values']['plain'];
  }

  function query() {
    $this->additional_fields[$this->field] = $this->field;
    $this->additional_fields['currency_code'] = 'currency_code';
    $this->additional_fields['type'] = 'type';
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $type = $values->{$this->aliases['type']};
    $currency_code = ($this->options['show_currency']) ? $values->{$this->aliases['currency_code']} : 'XXX';
    $value = $values->{$this->aliases[$this->field]};

    // Check if there is a value. If not, return NULL.
    if (is_null($value)) {
      return NULL;
    }

    // If the 'plain' option is checked, return the value in whole units (not cents).
    if ($this->options['plain']) {
      return $value / 100;
    }

    $result = Auction::formatPrice($value / 100, $currency_code);

    // Remove currency symbol if option 'show_currency' is disabled.
    if (!$this->options['show_currency']) {
      $result = trim(str_replace('¤', '', $result));
    }

    return $result;
  }

}
