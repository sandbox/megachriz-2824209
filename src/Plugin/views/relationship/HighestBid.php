<?php

namespace Drupal\auction\Plugin\views\relationship;

use \QueryAlterableInterface;
use \views_handler_relationship_groupwise_max;
use \views_join_subquery;

/**
 * Filters the view on auctions where the current user has a bid on.
 */
class HighestBid extends views_handler_relationship_groupwise_max {
  /**
   * Defines default values for options.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['bid_type'] = array('default' => 'standard');
    $options['status'] = array('default' => '1');

    // Remove non-relevant options.
    unset($options['subquery_sort']);
    unset($options['subquery_order']);
    unset($options['subquery_view']);

    return $options;
  }

  /**
   * Extends the relationship's options by selecting a bid type.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $bid_type_options = array(
      '' => t('Any'),
    );
    foreach (ctools_get_plugins('auction', 'bid_types') as $plugin_info) {
      $bid_type_options[$plugin_info['name']] = $plugin_info['name'];
    }
    $form['bid_type'] = array(
      '#title' => t('Bid type'),
      '#type' => 'select',
      '#options' => $bid_type_options,
      '#default_value' => $this->options['bid_type'],
    );

    $form['status'] = array(
      '#title' => t('Status'),
      '#type' => 'select',
      '#options' => array(
        '' => t('Any'),
        '0' => t('Inactive bids'),
        '1' => t('Active bids'),
      ),
      '#default_value' => $this->options['status'],
    );

    // Remove non-relevant options.
    unset($form['subquery_sort']);
    unset($form['subquery_order']);
    unset($form['subquery_view']);
  }

  /**
   * Overrides left_query() to completely generate the subquery in our own way.
   */
  function left_query($options) {
    $this->subquery_namespace = (!empty($options['subquery_namespace'])) ? '_'. $options['subquery_namespace'] : '_SUBQUERY';

    $subquery = db_select('auction_bids', 'auction_bids')
      ->fields('auction_bids', array('bid_id'))
      ->orderBy('auction_bids.amount', 'DESC')
      ->orderBy('auction_bids.created', 'ASC')
      ->orderBy('auction_bids.bid_id', 'ASC')
      ->addTag('auction_highest_bid');

    // Optionally filter on status.
    if (isset($this->options['status']) && $this->options['status'] !== '') {
      $subquery->where('auction_bids.status = ' . db_escape_field($this->options['status']));
    }

    // Join with auction table.
    // Add condition to the join in case a bid type is specified on the settings.
    $join_condition = 'auction_bids.auction_id = auction_auction_bids.auction_id';
    if ($this->options['bid_type']) {
      $join_condition .= ' AND auction_bids.type = \'' . $this->options['bid_type'] . '\'';
    }
    $subquery->join('auction', 'auction_auction_bids', $join_condition);

    $placeholder = ':' . $this->definition['argument table'] . '_' . $this->definition['argument field'];
    $subquery->where('auction_auction_bids.auction_id = ' . $placeholder, array(
      $placeholder => '**CORRELATED**',
    ));

    // ---------------------------------------------------------------------------
    // The next bit is taken from views_handler_relationship_groupwise_max.
    // ---------------------------------------------------------------------------

    // Workaround until http://drupal.org/node/844910 is fixed:
    // Remove all fields from the SELECT except the base id.
    $fields =& $subquery->getFields();
    foreach (array_keys($fields) as $field_name) {
      // The base id for this subquery is stored in our definition.
      if ($field_name != $this->definition['field']) {
        unset($fields[$field_name]);
      }
    }

    // Make every alias in the subquery safe within the outer query by
    // appending a namespace to it, '_inner' by default.
    $tables =& $subquery->getTables();
    foreach (array_keys($tables) as $table_name) {
      $tables[$table_name]['alias'] .= $this->subquery_namespace;
      // Namespace the join on every table.
      if (isset($tables[$table_name]['condition'])) {
        $tables[$table_name]['condition'] = $this->condition_namespace($tables[$table_name]['condition']);
      }
    }
    // Namespace fields.
    foreach (array_keys($fields) as $field_name) {
      $fields[$field_name]['table'] .= $this->subquery_namespace;
      $fields[$field_name]['alias'] .= $this->subquery_namespace;
    }
    // Namespace conditions.
    $where =& $subquery->conditions();
    $this->alter_subquery_condition($subquery, $where);

    $orders =& $subquery->getOrderBy();
    foreach ($orders as $order_key => $order) {
      list($sort_table, $sort_field) = explode('.', $order_key);
      $orders2[$sort_table . $this->subquery_namespace . '.' . $sort_field] = $order;
      unset($orders[$order_key]);
    }
    $orders = $orders2;

    // The query we get doesn't include the LIMIT, so add it here.
    $subquery->range(0, 1);

    // Extract the SQL the temporary view built.
    $subquery_sql = $subquery->__toString();

    // Replace the placeholder with the outer, correlated field.
    // Eg, change the placeholder ':users_uid' into the outer field 'users.uid'.
    // We have to work directly with the SQL, because putting a name of a field
    // into a SelectQuery that it does not recognize (because it's outer) just
    // makes it treat it as a string.
    $outer_placeholder = ':' . str_replace('.', '_', $this->definition['outer field']);
    $subquery_sql = str_replace($outer_placeholder, $this->definition['outer field'], $subquery_sql);

    return $subquery_sql;
  }

  /**
   * Overrides views_handler_relationship_groupwise_max::query().
   *
   * The cache ID is based on all set options, not just the option 'id'.
   */
  function query() {
    // Figure out what base table this relationship brings to the party.
    $table_data = views_fetch_data($this->definition['base']);
    $base_field = empty($this->definition['base field']) ? $table_data['table']['base']['field'] : $this->definition['base field'];

    $this->ensure_my_table();

    $def = $this->definition;
    $def['table'] = $this->definition['base'];
    $def['field'] = $base_field;
    $def['left_table'] = $this->table_alias;
    $def['left_field'] = $this->field;
    if (!empty($this->options['required'])) {
      $def['type'] = 'INNER';
    }

    if ($this->options['subquery_regenerate']) {
      // For testing only, regenerate the subquery each time.
      $def['left_query'] = $this->left_query($this->options);
    }
    else {
      // Get the stored subquery SQL string.
      $cid = 'views_relationship_groupwise_max:' . $this->view->name . ':' . $this->view->current_display . ':' . hash('md5', serialize($this->options));
      $cache = cache_get($cid, 'cache_views_data');
      if (isset($cache->data)) {
        $def['left_query'] = $cache->data;
      }
      else {
        $def['left_query'] = $this->left_query($this->options);
        cache_set($cid, $def['left_query'], 'cache_views_data');
      }
    }

    if (!empty($def['join_handler']) && class_exists($def['join_handler'])) {
      $join = new $def['join_handler'];
    }
    else {
      $join = new views_join_subquery();
    }

    $join->definition = $def;
    $join->construct();
    $join->adjusted = TRUE;

    // use a short alias for this:
    $alias = $def['table'] . '_' . $this->table;

    $this->alias = $this->query->add_relationship($alias, $join, $this->definition['base'], $this->relationship);
  }

}
