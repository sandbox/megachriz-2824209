<?php

namespace Drupal\auction\Plugin\views\argument;

use \views_handler_argument_numeric;

/**
 * Argument handler to filter auctions in which an user has bid.
 */
class UserPlacedBid extends views_handler_argument_numeric {
  public function query($group_by = FALSE) {
    $this->ensure_my_table();

    // Determine whether or not to exclude or not.
    $keyword = $this->options['not'] ? 'NOT EXISTS' : 'EXISTS';

    // Add expression.
    $this->query->add_where_expression(0, "$keyword (SELECT auction_id FROM auction_bids WHERE auction_id = $this->table_alias.auction_id AND uid = $this->argument)");
  }
}