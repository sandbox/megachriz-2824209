<?php

namespace Drupal\auction\Plugin\views\filter;

use \views_handler_filter_boolean_operator;

/**
 * Filters the view on auctions where the current user has a bid on.
 */
class UserPlacedBid extends views_handler_filter_boolean_operator {

  function query() {
    global $user;
    $this->ensure_my_table();

    if ($this->value) {
      $keyword = 'EXISTS';
    }
    else {
      $keyword = 'NOT EXISTS';
    }

    $this->query->add_where_expression($this->options['group'], "$keyword (SELECT * FROM auction_bids WHERE auction_id = $this->table_alias.auction_id AND uid = $user->uid)");
  }

}
