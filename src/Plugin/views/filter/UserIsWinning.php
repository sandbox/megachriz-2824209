<?php

namespace Drupal\auction\Plugin\views\filter;

use \views_handler_filter_boolean_operator;

/**
 * Filters the view on auctions where the current user is winning.
 */
class UserIsWinning extends views_handler_filter_boolean_operator {

  function query() {
    global $user;
    $this->ensure_my_table();

    if ($this->value) {
      $operator = '=';
    }
    else {
      $operator = '!=';
    }

    $this->query->add_where_expression($this->options['group'], "$user->uid $operator (SELECT uid FROM auction_bids WHERE auction_id = $this->table_alias.auction_id ORDER BY amount DESC LIMIT 0, 1)");
  }

}
