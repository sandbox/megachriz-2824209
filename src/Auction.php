<?php

namespace Drupal\auction;

use \RuntimeException;
use Drupal\auction\Entity\Auction as AuctionEntity;
use Drupal\auction\Plugin\auction\BidType\BidTypeInterface;

/**
 * Class for API functions.
 */
abstract class Auction {
  /**
   * Masks user name by leaving only first and last letter.
   *
   * @param string $name
   *   The name to mask.
   *
   * @return string
   *   The masked user name.
   */
  public static function maskUsername($name) {
    return $name[0] . '***' . $name[strlen($name) - 1];
  }

  /**
   * Checks if the first parameter can be cleanly divided.
   *
   * @param int $x
   *   The number to divide.
   * @param int $y
   *   The number to divide the first number width.
   *
   * @return bool
   *   True if the number is divisable.
   *   False otherwise.
   */
  public static function isDivisable($x, $y) {
    if ($y == 0) {
      return FALSE;
    }
    return abs(($x / $y) - round($x / $y, 0)) == 0;
  }

  /**
   * Acquire (or renew) a lock, blocking briefly on fail as lock is re-attempted.
   *
   * @param $name
   *   The name of the lock. Limit of name's length is 255 characters.
   * @param float $timeout
   *   A number of seconds (float) before the lock expires (minimum of 0.001).
   * @param int $delay
   *   The maximum number of seconds to wait, as an integer.
   *
   * @return bool
   *   TRUE if the lock was acquired, FALSE if it failed even after wait
   */
  public static function lockAcquire($name, $timeout = 30.0, $delay = 30) {
    return (
      lock_acquire($name, $timeout)
      || (
        !lock_wait($name, $delay)
        && lock_acquire($name, $timeout)
      )
    );
  }

  /**
   * Gets an instance of a class for a given bid type plugin.
   *
   * @param string $type
   *   A string that is the key of the plugin to load.
   * @param \Drupal\auction\Entity\Auction $auction
   *   The auction to pass to the plugin.
   *
   * @return Drupal\auction\Plugin\auction\BidType\BidTypeInterface
   *   A bid type plugin.
   */
  public static function getBidTypePlugin($type, AuctionEntity $auction) {
    ctools_include('plugins');

    $class = ctools_plugin_load_class('auction', 'bid_types', $type, 'handler');
    if (is_subclass_of($class, 'Drupal\\auction\\Plugin\\auction\\BidType\\BidTypeInterface')) {
      return new $class($type, $auction);
    }
    else {
      throw new RuntimeException(strtr('Bid type plugin !plugin not found.', array(
        '!plugin' => $type,
      )));
    }
  }

  /**
   * Returns instances for all bid type plugins.
   */
  public static function getAllBidTypePlugins(AuctionEntity $auction, $active_only = TRUE) {
    $return = array();

    foreach (ctools_get_plugins('auction', 'bid_types') as $plugin_info) {
      $plugin = static::getBidTypePlugin($plugin_info['name'], $auction);
      if (!$active_only || $plugin->isActive()) {
        $return[$plugin_info['name']] = $plugin;
      }
    }

    return $return;
  }

  /**
   * Returns auction controller.
   *
   * @return AuctionEntityController
   */
  public static function getController() {
    return entity_get_controller('auction');
  }

  public static function getHighestBidValue($auction) {
    return static::getController()->getHighestBid($auction)->amount;
  }

  public static function getSettings($property = NULL) {
    static $settings = NULL;
    if (is_null($settings)) {
      $settings = array(
        'currency' => variable_get('auction_currency', 'EUR'),
        'bid_step' => variable_get('auction_bid_step', 0.5),
        'buy_now_mode' => variable_get('auction_buy_now_mode', 'first bid'),
        'buy_now_threshold' => variable_get('auction_buy_now_threshold', 80),
        'usage_fee' => variable_get('auction_usage_fee', '15'),
        'provision_fee' => variable_get('auction_provision_fee', 0.05),
      );
    }

    if ($property === NULL) {
      return $settings;
    }

    return $settings[$property];
  }

  public static function statuses() {
    return array(
      AuctionEntity::STATUS_NOT_YET_STARTED => t('Not yet started'),
      AuctionEntity::STATUS_ACTIVE => t('Active'),
      AuctionEntity::STATUS_FINISHED => t('Finished'),
    );
  }

  public static function formatPrice($amount, $currency_code = 'XXX') {
    $currency = currency_load($currency_code);
    if (!$currency) {
      $currency = currency_load('XXX');
    }
    if (!$currency) {
      throw new RuntimeException(t('Could not load currency with ISO 4217 code XXX.'));
    }
    return $currency->format($amount);
  }
}
