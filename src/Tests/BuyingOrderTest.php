<?php

namespace Drupal\auction\Tests;

use \Drupal\auction\Auction;

/**
 * Tests if buying orders can be placed on auctions.
 *
 * @group auction
 */
class BuyingOrderTest extends TestBase {
  /**
   * An auction node.
   *
   * @var object
   */
  protected $auctionNode;

  /**
   * A customer who may place bids.
   *
   * @var object
   */
  protected $customerA;

  /**
   * An other customer who may place bids.
   *
   * @var object
   */
  protected $customerB;

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Buying Order Test',
      'description' => 'Tests if buying orders can be placed on auctions.',
      'group' => 'Auction',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp(array $modules = array(), array $permissions = array()) {
    parent::setUp($modules, $permissions);

    // Set bid increment and minimum bid increase to 5.
    variable_set('auction_bid_step', 5);

    // Create auction node with auction.
    $this->auctionNode = $this->createNodeWithAuction(array(
      'starting_price' => 50,
    ));

    // Create two users that may bid.
    $this->customerA = $this->createUserWithPermissionHelper(array('store customer', 'auction customer'));
    $this->customerA->name = 'CustomerA';
    $this->customerA->mail = 'CustomerA@example.com';
    user_save($this->customerA);
    $this->customerB = $this->createUserWithPermissionHelper(array('store customer', 'auction customer'));
    $this->customerB->name = 'CustomerB';
    $this->customerB->mail = 'CustomerB@example.com';
    user_save($this->customerB);

    // Login as user who may bid.
    $this->drupalLogin($this->customerA);
  }

  /**
   * {@inheritdoc}
   */
  public function tearDown() {
    // Display mails that were sent.
    $mails = $this->drupalGetMails();

    $table = array(
      '#theme' => 'table',
      '#header' => array(
        'from' => 'From',
        'to' => 'To',
        'subject' => 'Subject',
        'body' => 'Body',
      ),
      '#rows' => array(),
    );
    foreach ($mails as $mail) {
      $table['#rows'][] = array(
        'from' => $mail['from'],
        'to' => $mail['to'],
        'subject' => $mail['subject'],
        'body' => $mail['body'],
      );
    }

    $this->verbose(drupal_render($table));

    return parent::tearDown();
  }

  // Running all tests takes about 20 minutes. A single one takes 2.5 minutes.
  public function runAll(array $methods = array()) {
    $methods = ['testPlaceBuyingOrderOneStepInBetweenBidAndBuyingOrder'];
    return parent::run($methods);
  }

  /**
   * Places a buying order.
   *
   * @param int $nid
   *   ID of the auction node to place a bid on.
   * @param float $amount
   *   The amount to bid.
   * @param bool $confirm
   *   (optional) Whether or not to confirm the bid.
   *   Defaults to TRUE.
   * @param bool $assert_success
   *   (optional) Whether or not to assert success of the action.
   *   Defaults to TRUE.
   */
  protected function placeBuyingOrder($nid, $amount, $confirm = TRUE, $assert_success = TRUE) {
    $edit = array(
      'bid_amount' => $amount,
      'form_id' => 'auction_bids_form',
    );
    $this->drupalPost('node/' . $nid, $edit, t('Place buying order'));

    if ($confirm) {
      $this->drupalPost(NULL, array(), t('Confirm'));
    }

    if ($assert_success) {
      $this->assertPlaceBuyingOrder($nid, $amount);
    }
  }

  /**
   * Asserts that a text exists in the current page by the given XPath.
   *
   * @param $xpath
   *   XPath used to find the text.
   * @param $value
   *   Value of the text to assert.
   * @param $message
   *   (optional) Message to display.
   * @param $group
   *   (optional) The group this message belongs to.
   *
   * @return
   *   TRUE on pass, FALSE on fail.
   */
  protected function assertTextByXpath($xpath, $text, $message = '', $group = 'Other') {
    if (empty($message)) {
      $message = '"@text" found by xpath (actual: @actual).';
    }

    $elements = $this->xpath($xpath);
    $text2 = (string) $elements[0];
    $text2 = trim($text2);

    // Pass in variables for the message.
    $message = format_string($message, array(
      '@text' => $text,
      '@actual' => $text2,
    ));

    return $this->assertEqual($text, $text2, $message, $group);
  }

  /**
   * Asserts that a mail was sent to the given mail address
   *
   * @param string $subject
   *   The expected ID of the mail that should have been sent.
   * @param string $recipient
   *   The recipient of the mail.
   * @param int $count
   *   (optional) The expected number of times that this mail has been sent.
   *   Defaults to 1.
   * @param string $message
   *   Message to display.
   *
   * @return
   *   TRUE on pass, FALSE on fail.
   */
  protected function assertMailSent($subject, $recipient, $count = 1, $message = '') {
    if (empty($message)) {
      if ($count === 1) {
        $message = '1 mail with subject "@subject" was sent to @recipient (actual: @actual_number).';
      }
      elseif (!is_null($count)) {
        $message = '@number mails with subject "@subject" were sent to @recipient (actual: @actual_number).';
      }
      else {
        $message = 'At least one mail with subject "@subject" was sent to @recipient.';
      }
    }

    $mails = $this->drupalGetMails(array(
      'subject' => $subject,
      'to' => $recipient,
    ));

    // Pass in variables for the message.
    $message = format_string($message, array(
      '@number' => $count,
      '@subject' => $subject,
      '@recipient' => $recipient,
      '@actual_number' => count($mails),
    ));

    if (!is_null($count)) {
      $this->assertEqual($count, count($mails), $message);
    }
    else {
      $this->assertTrue(count($mails) > 0, $message);
    }
  }

  /**
   * Asserts that a buying order was saved.
   *
   * @param int $nid
   *   ID of the auction node were a bid was placed for.
   * @param float $amount
   *   The expected amount that was bid.
   * @param int $bid_id
   *   (optional) ID of bid entity.
   *   Defaults to latest placed bid.
   */
  protected function assertPlaceBuyingOrder($nid, $amount, $bid_id = NULL) {
    $amount_in_cents = $amount * 100;
    $amount_display = Auction::formatPrice($amount, 'EUR');

    $auction = $this->getAuctionFromNode($nid, TRUE);
    $bids = $auction->getBids('buying_order');
    $bid = reset($bids);

    // Assert that the bid is saved.
    $this->assertEqual($amount_in_cents, $bid->amount, format_string('The buying order was placed with an amount of @amount (actual: @actual).', array(
      '@amount' => $amount_in_cents,
      '@actual' => $bid->amount,
    )));

    // Assert that it is displayed as well.
    $this->assertText(t('Your buying order has been placed.'));
    // Assert that the customer sees its own buying order amount.
    $xpath_query = '//div[@id=\'edit-amount\']/text()';
    $this->assertTextByXpath($xpath_query, $amount_display);
  }

  /**
   * Basic test for placing a buying order.
   */
  public function testPlaceBuyingOrder() {
    // Place a buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 1000);

    // Assert that the system placed a bid of 50 for the customer.
    $this->assertPlaceBid($this->auctionNode->nid, 50, $this->customerA->uid, FALSE);

    // Assert current price.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual(50, $auction->getCurrentPrice());

    // Assert that the current price is displayed as well.
    $xpath_query = '//div[@id=\'edit-current-price\']/text()';
    $amount_display = Auction::formatPrice(50, 'EUR');
    $this->assertTextByXpath($xpath_query, $amount_display);

    // Assert that customer A received two mails, one for bid and one for buying
    // order.
    $this->assertMailSent('Your buying order has been placed', $this->customerA->mail, 1);
    // @todo not sent.
    $this->assertMailSent('Your bid has been placed', $this->customerA->mail, 1);

    // Assert that the author of the auction only received one mail about the
    // bid.
    $auction_author = user_load(1);
    $this->assertMailSent('Bid placed in your auction.', $auction_author->mail, 1);

    // Let the auction end.
    $this->endAuctionByNode($this->auctionNode);
    // Assert that customer won auction with a bid of 50.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual($this->customerA->uid, $auction->getWinningUserId());
    $this->assertEqual(50, Auction::getController()->determineFinishedAuctionPrice($auction));
  }

  /**
   * Tests placing a buying order on an auction that already has a bid.
   */
  public function testPlaceBuyingOrderWithExistingBids() {
    // Customer A places a bid of 50.
    $this->placeBid($this->auctionNode->nid, 50);

    // Login as customer B.
    $this->drupalLogin($this->customerB);

    // Customer B places a buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 1000);

    // Assert that system placed a bid of 55 for customer B.
    $this->assertPlaceBid($this->auctionNode->nid, 55, $this->customerB->uid, FALSE);
    // Assert that customer B received two mails, one for bid and one for buying
    // order.
    $this->assertMailSent('Your buying order has been placed', $this->customerB->mail, 1);
    // @todo not sent.
    $this->assertMailSent('Your bid has been placed', $this->customerB->mail, 1);

    // Assert that customer A received a mail of being outbid.
    $this->assertMailSent('You have been outbid', $this->customerA->mail, 1);

    // Let auction end.
    $this->endAuctionByNode($this->auctionNode);

    // Assert that customer B won auction with a bid of 55.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual($this->customerB->uid, $auction->getWinningUserId());
    $this->assertEqual(55, Auction::getController()->determineFinishedAuctionPrice($auction));
  }

  /**
   * Tests placing a bid on an auction that has a buying order, but the bid is
   * lower than the buying order's maximum.
   *
   * The system should place a new bid for the customer who placed the buying
   * order and the customer who placed the bid should immediately be outbid.
   */
  public function testPlaceBidBelowBuyingOrder() {
    // First, let customer A place a buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 1000);
    $this->assertMailSent('Your buying order has been placed', $this->customerA->mail, 1);

    // Assert that system placed a bid of 50 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 50, $this->customerA->uid, FALSE);

    // Login as customer B.
    $this->drupalLogin($this->customerB);

    // Go to auction and assert that customer B cannot see buying order amount.
    $this->drupalGet('node/' . $this->auctionNode->nid);
    $this->assertNoText(Auction::formatPrice(1000, 'EUR'));

    // Customer B places a bid of 900.
    $this->placeBid($this->auctionNode->nid, 900, TRUE, FALSE);
    $auction = $this->getAuctionFromNode($this->auctionNode->nid, TRUE);
    $this->assertBidValues($auction->getBidAtIndex(1, 'standard'), 900);

    // Assert that system placed a bid of 905 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 905, $this->customerA->uid, FALSE);
    // Assert that customer B sees that he has been outbid.
    $this->assertText('You have been outbid');
    $this->assertMailSent('You have been outbid', $this->customerB->mail, 1);

    // Assert that customer A received mail about new bid placed, but none about
    // being outbid.
    $this->assertMailSent('You have been outbid', $this->customerA->mail, 0);
    $this->assertMailSent('A new bid was placed', $this->customerA->mail, 1);

    // Let auction end.
    $this->endAuctionByNode($this->auctionNode);
    // Assert that customer A won auction with a bid of 905.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual($this->customerA->uid, $auction->getWinningUserId());
    $this->assertEqual(905, Auction::getController()->determineFinishedAuctionPrice($auction));
  }

  /**
   * Tests placing a bid on an auction that has a buying order, but the bid is
   * equal to the buying order's maximum.
   *
   * The system should place a new bid for customer A (who placed the buying
   * order) that is equal to the bid just placed by customer B. Customer B
   * should get a message that he is outbid.
   */
  public function testPlaceBidEqualToBuyingOrder() {
    // First, let customer A place a buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 1000);

    // Assert that system placed a bid of 50 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 50, $this->customerA->uid, FALSE);

    // Login as customer B.
    $this->drupalLogin($this->customerB);

    // Customer B places a bid of 1000.
    $this->placeBid($this->auctionNode->nid, 1000, TRUE, FALSE);
    $auction = $this->getAuctionFromNode($this->auctionNode->nid, TRUE);
    $this->assertBidValues($auction->getBidAtIndex(1, 'standard'), 1000);

    // Assert that system placed a bid of 1000 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 1000, $this->customerA->uid, FALSE);
    // Assert that customer B sees that he has been outbid.
    $this->assertText('You have been outbid');
    $this->assertText('because this user bet the same amount first');
    $this->assertMailSent('You have been outbid', $this->customerB->mail, 1);

    // Assert that customer A received mail about new bid placed, but none about
    // being outbid.
    $this->assertMailSent('You have been outbid', $this->customerA->mail, 0);
    $this->assertMailSent('A new bid was placed', $this->customerA->mail, 1);

    // Let auction end.
    $this->endAuctionByNode($this->auctionNode);
    // Assert that customer A won auction with a bid of 1000.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual($this->customerA->uid, $auction->getWinningUserId());
    $this->assertEqual(1000, Auction::getController()->determineFinishedAuctionPrice($auction));
  }

  /**
   * Tests placing a bid on an auction that has a buying order, but the bid is
   * higher than the buying order's maximum.
   *
   * The system should send a mail to customer A that he has been outbid.
   */
  public function testPlaceBidAboveBuyingOrder() {
    // First, let customer A place a buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 1000);

    // Assert that system placed a bid of 50 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 50, $this->customerA->uid, FALSE);

    // Login as customer B.
    $this->drupalLogin($this->customerB);

    // Customer B places a bid of 1005, just above the buying order amount.
    $this->placeBid($this->auctionNode->nid, 1005);
    // Assert that customer B only sees that the bid was successfully placed.
    $this->assertNoText('You have been outbid');

    // Assert that the buying order placed by customer A is not shown.
    $this->assertNoText(Auction::formatPrice(1000, 'EUR'));

    // Assert that customer B received a mail about that his bid was placed.
    $this->assertMailSent('Your bid has been placed', $this->customerB->mail, 1);

    // Assert that customer A received a mail about that he has been being
    // outbid.
    $this->assertMailSent('Your buying order has been outbid', $this->customerA->mail, 1);
    $this->assertMailSent('You have been outbid', $this->customerA->mail, 0);

    // Let auction end.
    $this->endAuctionByNode($this->auctionNode);
    // Assert that customer B won auction with a bid of 1005.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual($this->customerB->uid, $auction->getWinningUserId());
    $this->assertEqual(1005, Auction::getController()->determineFinishedAuctionPrice($auction));
  }

  /**
   * Tests placing a buying order on an auction that has a buying order, but the
   * new buying order amount is lower than the other buying order's maximum.
   *
   * The system should place a new bid for customer A (who placed the first
   * buying order) and customer B (who placed the second buying order) should
   * immediately be outbid.
   */
  public function testPlaceBuyingOrderBelowExistingBuyingOrder() {
    // First, let customer A place a buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 100);

    // Assert that system placed a bid of 50 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 50, $this->customerA->uid, FALSE);

    // Login as customer B.
    $this->drupalLogin($this->customerB);

    // Customer B places a buying order of 75, below the buying order amount of
    // customer A.
    $this->placeBuyingOrder($this->auctionNode->nid, 75, TRUE, FALSE);

    // Assert that system placed a bid of 80 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 80, $this->customerA->uid, FALSE);

    // Assert that bids are placed between the previous highest bid and the
    // buying order of customer B.
    $bids = array_reverse($this->getAuctionFromNode($this->auctionNode->nid, TRUE)
      ->getBids('standard')
    );
    $this->assertBidValues($bids[0], 50, $this->customerA->uid);
    $this->assertBidValues($bids[1], 55, $this->customerB->uid);
    $this->assertBidValues($bids[2], 60, $this->customerA->uid);
    $this->assertBidValues($bids[3], 65, $this->customerB->uid);
    $this->assertBidValues($bids[4], 70, $this->customerA->uid);
    $this->assertBidValues($bids[5], 75, $this->customerB->uid);
    $this->assertBidValues($bids[6], 80, $this->customerA->uid);

    // And assert that no more bids exist.
    $this->assertFalse(isset($bids[7]), 'There are 7 bids in total.');

    // Assert that customer B sees that he has been outbid.
    $this->assertText('You have been outbid');
    $this->assertMailSent('Your buying order has been outbid', $this->customerB->mail, 1);
    $this->assertMailSent('You have been outbid', $this->customerB->mail, 0);

    // Assert that customer A received mail about new bid placed, but none about
    // being outbid.
    $this->assertMailSent('You have been outbid', $this->customerA->mail, 0);
    $this->assertMailSent('A new bid was placed', $this->customerA->mail, 1);

    // Let auction end.
    $this->endAuctionByNode($this->auctionNode);
    // Assert that customer A won auction with a bid of 80.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual($this->customerA->uid, $auction->getWinningUserId());
    $this->assertEqual(80, Auction::getController()->determineFinishedAuctionPrice($auction));
  }

  /**
   * Tests placing a buying order just one bid step below an existing buying
   * order.
   */
  public function testPlaceBuyingOrderOneStepBelowExistingBuyingOrder() {
    // First, let customer A place a buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 100);

    // Assert that system placed a bid of 50 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 50, $this->customerA->uid, FALSE);

    // Login as customer B.
    $this->drupalLogin($this->customerB);

    // Customer B places a buying order of 95, just one step below the
    // buying order amount of customer A.
    $this->placeBuyingOrder($this->auctionNode->nid, 95, TRUE, FALSE);

    // Assert that system placed a bid of 100 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 100, $this->customerA->uid, FALSE);

    // Assert that bids are placed between the previous highest bid and the bid
    // order of customer B.
    $bids = array_reverse($this->getAuctionFromNode($this->auctionNode->nid, TRUE)
      ->getBids('standard')
    );
    $this->assertBidValues($bids[0], 50, $this->customerA->uid);
    $this->assertBidValues($bids[1], 55, $this->customerB->uid);
    $this->assertBidValues($bids[2], 60, $this->customerA->uid);
    $this->assertBidValues($bids[3], 65, $this->customerB->uid);
    $this->assertBidValues($bids[4], 70, $this->customerA->uid);
    $this->assertBidValues($bids[5], 75, $this->customerB->uid);
    $this->assertBidValues($bids[6], 80, $this->customerA->uid);
    $this->assertBidValues($bids[7], 85, $this->customerB->uid);
    $this->assertBidValues($bids[8], 90, $this->customerA->uid);
    $this->assertBidValues($bids[9], 95, $this->customerB->uid);
    $this->assertBidValues($bids[10], 100, $this->customerA->uid);

    // And assert that no more bids exist.
    $this->assertFalse(isset($bids[11]), 'There are 11 bids in total.');

    // Assert that customer B sees that he has been outbid.
    $this->assertText('You have been outbid');
    $this->assertMailSent('Your buying order has been outbid', $this->customerB->mail, 1);
    $this->assertMailSent('You have been outbid', $this->customerB->mail, 0);

    // Assert that customer A received mail about new bid placed, but none about
    // being outbid.
    $this->assertMailSent('You have been outbid', $this->customerA->mail, 0);
    $this->assertMailSent('A new bid was placed', $this->customerA->mail, 1);

    // Let auction end.
    $this->endAuctionByNode($this->auctionNode);
    // Assert that customer A won auction with a bid of 100.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual($this->customerA->uid, $auction->getWinningUserId());
    $this->assertEqual(100, Auction::getController()->determineFinishedAuctionPrice($auction));
  }

  /**
   * Tests placing a buying order just one bid step below an existing buying
   * order and one step above an existing bid.
   */
  public function testPlaceBuyingOrderOneStepInBetweenBidAndBuyingOrder() {
    // First, let customer A place a bid of 85.
    $this->placeBid($this->auctionNode->nid, 85);

    // Login as customer B.
    $this->drupalLogin($this->customerB);

    // Customer B places a buying order of 100.
    $this->placeBuyingOrder($this->auctionNode->nid, 100);

    // Assert that system placed a bid of 90 for customer B.
    $this->assertPlaceBid($this->auctionNode->nid, 90, $this->customerB->uid, FALSE);

    // Now log in as customer A again.
    $this->drupalLogin($this->customerA);

    // Customer A places a buying order of 95, just one step below the buying order
    // of customer B *and* one step above the current bid.
    $this->placeBuyingOrder($this->auctionNode->nid, 95, TRUE, FALSE);

    // Assert that system placed a bid of 100 for customer B.
    $this->assertPlaceBid($this->auctionNode->nid, 100, $this->customerB->uid, FALSE);

    // Assert the placed bids.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid, TRUE);
    entity_get_controller('auction')->attachBids($auction);
    $bids = array_reverse($auction->getBids('standard'));
    $this->assertBidValues($bids[0], 85, $this->customerA->uid);
    $this->assertBidValues($bids[1], 90, $this->customerB->uid);
    $this->assertBidValues($bids[2], 95, $this->customerA->uid);
    $this->assertBidValues($bids[3], 100, $this->customerB->uid);

    // And assert that no more bids exist.
    $this->assertFalse(isset($bids[4]), 'There are 4 bids in total.');

    // Assert that customer A sees that he has been outbid.
    $this->assertText('You have been outbid');
    $this->assertMailSent('Your buying order has been outbid', $this->customerA->mail, 1);
    $this->assertMailSent('You have been outbid', $this->customerA->mail, 1);

    // Assert that customer B received mail about new bid placed, but none about
    // being outbid.
    $this->assertMailSent('You have been outbid', $this->customerB->mail, 0);
    $this->assertMailSent('A new bid was placed', $this->customerB->mail, 1);

    // Let auction end.
    $this->endAuctionByNode($this->auctionNode);
    // Assert that customer B won auction with a bid of 100.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual($this->customerB->uid, $auction->getWinningUserId());
    $this->assertEqual(100, Auction::getController()->determineFinishedAuctionPrice($auction));
  }

  /**
   * Tests placing a buying order on an auction that has a buying order, but the
   * new buying order amount is equal to the other buying order's maximum.
   *
   * The system should place a new bid for customer A (who placed the first bid
   * order) that is equal to the buying order's maximum just placed by customer
   * B. Customer B should get a message that he has been outbid.
   */
  public function testPlaceBuyingOrderEqualToExistingBuyingOrder() {
    // First, let customer A place a buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 100);

    // Assert that system placed a bid of 50 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 50, $this->customerA->uid, FALSE);

    // Login as customer B.
    $this->drupalLogin($this->customerB);

    // Customer B places a buying order of 100.
    $this->placeBuyingOrder($this->auctionNode->nid, 100, TRUE, FALSE);

    // Assert that system placed a bid of 100 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 100, $this->customerA->uid, FALSE);

    // Assert that bids are placed between the previous highest bid and the bid
    // order of customer B.
    $bids = array_reverse($this->getAuctionFromNode($this->auctionNode->nid, TRUE)
      ->getBids('standard')
    );
    $this->assertBidValues($bids[0], 50, $this->customerA->uid);
    $this->assertBidValues($bids[1], 55, $this->customerB->uid);
    $this->assertBidValues($bids[2], 60, $this->customerA->uid);
    $this->assertBidValues($bids[3], 65, $this->customerB->uid);
    $this->assertBidValues($bids[4], 70, $this->customerA->uid);
    $this->assertBidValues($bids[5], 75, $this->customerB->uid);
    $this->assertBidValues($bids[6], 80, $this->customerA->uid);
    $this->assertBidValues($bids[7], 85, $this->customerB->uid);
    $this->assertBidValues($bids[8], 90, $this->customerA->uid);
    $this->assertBidValues($bids[9], 95, $this->customerB->uid);
    $this->assertBidValues($bids[10], 100, $this->customerB->uid);
    $this->assertBidValues($bids[11], 100, $this->customerA->uid);

    // And assert that no more bids exist.
    $this->assertFalse(isset($bids[12]), 'There are 12 bids in total.');

    // Assert that customer B sees that he has been outbid.
    $this->assertText('You have been outbid');
    $this->assertText('because this user bet the same amount first');
    $this->assertMailSent('Your buying order has been outbid', $this->customerB->mail, 1);
    $this->assertMailSent('You have been outbid', $this->customerB->mail, 0);

    // Assert that customer A received mail about new bid placed, but none about
    // being outbid.
    $this->assertMailSent('You have been outbid', $this->customerA->mail, 0);
    $this->assertMailSent('A new bid was placed', $this->customerA->mail, 1);

    // Let auction end.
    $this->endAuctionByNode($this->auctionNode);
    // Assert that customer A won auction with a bid of 100.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual($this->customerA->uid, $auction->getWinningUserId());
    $this->assertEqual(100, Auction::getController()->determineFinishedAuctionPrice($auction));
  }

  /**
   * Tests placing a buying order on an auction that has a buying order, but the
   * new buying order amount is higher than the other buying order's maximum.
   *
   * The system should place a bid for customer B (who placed the second buying
   * order) that is above the buying order's maximum from customer A.
   * Customer A should get a message that he has been outbid.
   */
  public function testPlaceBuyingOrderAboveExistingBuyingOrder() {
    // First, let customer A place a buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 100);

    // Assert that system placed a bid of 50 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 50, $this->customerA->uid, FALSE);

    // Login as customer B.
    $this->drupalLogin($this->customerB);

    // Customer B places a buying order of 120, higher than the buying order
    // from customer A.
    $this->placeBuyingOrder($this->auctionNode->nid, 120);

    // Assert that system placed a bid of 105 for customer B.
    $this->assertPlaceBid($this->auctionNode->nid, 105, $this->customerB->uid, FALSE);
    // Assert that the buying order placed by customer A is now visible.
    $this->assertText(Auction::formatPrice(100, 'EUR'));

    // Assert that bids are placed between the previous highest bid and the
    // buying order of customer A.
    $bids = array_reverse($this->getAuctionFromNode($this->auctionNode->nid, TRUE)
      ->getBids('standard')
    );
    $this->assertBidValues($bids[0], 50, $this->customerA->uid);
    $this->assertBidValues($bids[1], 55, $this->customerB->uid);
    $this->assertBidValues($bids[2], 60, $this->customerA->uid);
    $this->assertBidValues($bids[3], 65, $this->customerB->uid);
    $this->assertBidValues($bids[4], 70, $this->customerA->uid);
    $this->assertBidValues($bids[5], 75, $this->customerB->uid);
    $this->assertBidValues($bids[6], 80, $this->customerA->uid);
    $this->assertBidValues($bids[7], 85, $this->customerB->uid);
    $this->assertBidValues($bids[8], 90, $this->customerA->uid);
    $this->assertBidValues($bids[9], 95, $this->customerB->uid);
    $this->assertBidValues($bids[10], 100, $this->customerA->uid);
    $this->assertBidValues($bids[11], 105, $this->customerB->uid);

    // And assert that no more bids exist.
    $this->assertFalse(isset($bids[12]), 'There are 12 bids in total.');

    // Assert that customer B only sees that the bid was successfully placed.
    $this->assertNoText('You have been outbid');

    // Assert that customer B received a mail about that his bid and buying
    // order was placed.
    $this->assertMailSent('Your buying order has been placed', $this->customerB->mail, 1);
    $this->assertMailSent('A new bid was placed', $this->customerB->mail, 1);

    // Assert that customer A received a mail about that he has been being
    // outbid.
    $this->assertMailSent('Your buying order has been outbid', $this->customerA->mail, 1);
    $this->assertMailSent('You have been outbid', $this->customerA->mail, 0);

    // Let auction end.
    $this->endAuctionByNode($this->auctionNode);
    // Assert that customer B won auction with a bid of 1005.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual($this->customerB->uid, $auction->getWinningUserId());
    $this->assertEqual(105, Auction::getController()->determineFinishedAuctionPrice($auction));
  }

  /**
   * Tests confirming a buying order when a higher bid is placed during that process.
   */
  public function testPlaceBuyingOrderWhenOverbid() {
    // Go to the form to place a buying order, but don't confirm yet.
    $this->placeBuyingOrder($this->auctionNode->nid, 500, FALSE, FALSE);

    // Create a bid for customer B above buying order.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $bid = entity_create('auction_bid', array(
      'auction_id' => $auction->identifier(),
      'amount' => 90000,
      'currency_code' => 'EUR',
      'uid' => $this->customerB->uid,
      'type' => 'standard',
      'source' => 'standard',
    ));
    Auction::getController()->saveBid($bid);

    // Now confirm placing the buying order.
    $this->drupalPost(NULL, array(), t('Confirm'));

    // Assert placed bids. Bidder A will have a bid of 500, while bidder B will have
    // a bid of 900.
    $bids = array_reverse($this->getAuctionFromNode($this->auctionNode->nid, TRUE)
      ->getBids('standard')
    );
    $this->assertBidValues($bids[0], 500, $this->customerA->uid);
    $this->assertBidValues($bids[1], 900, $this->customerB->uid);

    // And assert that no more bids exist.
    $this->assertFalse(isset($bids[2]), 'There are 2 bids in total.');

    // Let auction end.
    $this->endAuctionByNode($this->auctionNode);
    // Assert that customer B won auction with a bid of 900.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual($this->customerB->uid, $auction->getWinningUserId());
    $this->assertEqual(900, Auction::getController()->determineFinishedAuctionPrice($auction));
  }

  /**
   * Tests if in between bids are saved where the two last bids are the same.
   *
   * The in between bids are alternated between the two bidders until the
   * maximum bid of the outbid user is reached. If this bid is from the person
   * who just placed the higher buying order, then a bid from the outbid user
   * will follow with the same amount. The bid placing ends with a higher bid
   * from the highest bidder.
   * Example:
   * 1. Starting price is 50 and bid step is 5.
   * 2. Customer A places a buying order of 95. System places a bid for customer
   *    A of 50 (the starting price).
   * 3. Customer B places a buying order of 120.
   * 4. Bids between 50 and 100 are placed by the system, which will result to
   *    the following bids:
   *    B: 55
   *    A: 60
   *    B: 65
   *    A: 70
   *    B: 75
   *    A: 80
   *    B: 85
   *    A: 90
   *    B: 95
   *    A: 95
   *    B: 100
   *    Since the buying order of A was 95, there will be two bids of 95 here.
   */
  public function testShowBidsInBetweenSameLastBid() {
    // First, let customer A place a buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 95);

    // Assert that system placed a bid of 50 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 50, $this->customerA->uid, FALSE);

    // Login as customer B.
    $this->drupalLogin($this->customerB);

    // Customer B places a buying order of 120, higher than the buying order
    // from customer A.
    $this->placeBuyingOrder($this->auctionNode->nid, 120);

    // Assert that system placed a bid of 100 for customer B.
    $this->assertPlaceBid($this->auctionNode->nid, 100, $this->customerB->uid, FALSE);

    // Assert that all bids in between are saved, alternately between bidder A
    // and B.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid, TRUE);
    $bids = $auction->getBids('standard');
    $bids = array_reverse($bids);

    $current_bid = 50;
    $bid_step = 5;
    $uid = $this->customerA->uid;
    for ($i = 0; $current_bid < 95; $i++) {
      $this->assertBidValues($bids[$i], $current_bid, $uid);

      // Change values.
      $current_bid += $bid_step;
      $uid = ($uid == $this->customerA->uid) ? $this->customerB->uid : $this->customerA->uid;
    }

    // And assert that A and B both placed a bid of 95.
    $this->assertBidValues($bids[$i], 95, $this->customerB->uid);
    $this->assertBidValues($bids[$i + 1], 95, $this->customerA->uid);
    $this->assertBidValues($bids[$i + 2], 100, $this->customerB->uid);
  }

  /**
   * Tests if in between bids are saved where the two last bids are different.
   *
   * Just as in the previous test, the bids are alternated between the two
   * bidders. The difference is that if the maximum bid of the outbid user is
   * reached by the outbid user itself, there will be no two bids of the same
   * amount. Just as in the previous test, the bid placing ends with a higher
   * bid from the highest bidder.
   *
   * Example:
   * 1. Starting price is 50 and bid step is 5.
   * 2. Customer A places a buying order of 80. System places a bid for customer
   *    A of 50 (the starting price).
   * 3. Customer B places a buying order of 120.
   * 4. Bids between 50 and 85 are placed by the system, which will result to
   *    the following bids:
   *    B: 55
   *    A: 60
   *    B: 65
   *    A: 70
   *    B: 75
   *    A: 80
   *    B: 85
   */
  public function testShowBidsInBetweenDifferentLastBid() {
    // First, let customer A place a buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 80);

    // Assert that system placed a bid of 50 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 50, $this->customerA->uid, FALSE);

    // Login as customer B.
    $this->drupalLogin($this->customerB);

    // Customer B places a buying order of 120, higher than the buying order
    // from customer A.
    $this->placeBuyingOrder($this->auctionNode->nid, 120);

    // Assert that system placed a bid of 100 for customer B.
    $this->assertPlaceBid($this->auctionNode->nid, 85, $this->customerB->uid, FALSE);

    // Assert that all bids in between are saved, alternately between bidder A
    // and B.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid, TRUE);
    $bids = $auction->getBids('standard');
    $bids = array_reverse($bids);

    $current_bid = 50;
    $bid_step = 5;
    $uid = $this->customerA->uid;
    for ($i = 0; $current_bid < 80; $i++) {
      $this->assertBidValues($bids[$i], $current_bid, $uid);

      // Change values.
      $current_bid += $bid_step;
      $uid = ($uid == $this->customerA->uid) ? $this->customerB->uid : $this->customerA->uid;
    }

    // And assert that only A placed a bid of 80.
    $this->assertBidValues($bids[$i], 80, $this->customerA->uid);
    $this->assertBidValues($bids[$i + 1], 85, $this->customerB->uid);
  }

  /**
   * Tests if an user can edits his/her buying order by higher up the amount.
   */
  public function testEditBuyingOrderWithHigherAmount() {
    // Create a buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 1000);
    // Assert that system placed a bid of 50 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 50, $this->customerA->uid, FALSE);
    // Adjust the amount of the buying order by placing another buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 1200);

    // Assert that instead of a new buying order, the amount of the previous
    // buying order was adjusted.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual(1, count($auction->getBids('buying_order')), 'Only one buying order exists for the auction.');

    // Let auction end.
    $this->endAuctionByNode($this->auctionNode);
    // Assert that customer A won auction with a bid of 50.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual($this->customerA->uid, $auction->getWinningUserId());
    $this->assertEqual(50, Auction::getController()->determineFinishedAuctionPrice($auction));
  }

  /**
   * Tests if an user can edits his/her buying order by lowering the amount.
   */
  public function testEditBuyingOrderWithLowerAmount() {
    // Create a buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 1000);
    // Assert that system placed a bid of 50 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 50, $this->customerA->uid, FALSE);
    // Adjust the amount of the buying order by placing another buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 800);

    // Assert that instead of a new buying order, the amount of the previous
    // buying order was adjusted.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual(1, count($auction->getBids('buying_order')), 'Only one buying order exists for the auction.');
    // Assert also that the highest bid amount is 800.
    $this->assertEqual(800, $auction->getHighestBidAmount('buying_order'));

    // Let auction end.
    $this->endAuctionByNode($this->auctionNode);
    // Assert that customer A won auction with a bid of 50.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual($this->customerA->uid, $auction->getWinningUserId());
    $this->assertEqual(50, Auction::getController()->determineFinishedAuctionPrice($auction));
  }

  /**
   * Tests that an user ends up being outbid when it lowers his/her buying order
   * so that effectively it becomes lower than a bid being placed at the same
   * time.
   */
  public function _testEditBuyingOrderBelowOtherBid() {
    // @todo
    // Create a buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 1000);
    // Assert that system placed a bid of 50 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 50, $this->customerA->uid, FALSE);

    // Lower the buying order but do not confirm it yet.
    $this->placeBuyingOrder($this->auctionNode->nid, 800, FALSE, FALSE);

    // Create a bid for customer B below buying order.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $bid = entity_create('auction_bid', array(
      'auction_id' => $auction->identifier(),
      'amount' => 90000,
      'currency_code' => 'EUR',
      'uid' => $this->customerB->uid,
      'type' => 'standard',
      'source' => 'standard',
    ));
    Auction::getController()->saveBid($bid);

    // Now confirm the buying order.
    $this->drupalPost(NULL, array(), t('Confirm'));

    // @todo asserts.
  }

  /**
   * Tests if an user can remove his/her buying order.
   */
  public function testRemoveBuyingOrder() {
    // Create a buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 1000);
    // Assert that system placed a bid of 50 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 50, $this->customerA->uid, FALSE);

    // And remove it again.
    $this->clickLink(t('Remove buying order'));
    $this->drupalPost(NULL, array(), t('Confirm'));

    // Assert message and current path.
    $this->assertText(t('Your buying order has been removed.'));
    $this->assertEqual($this->getAbsoluteUrl('node/' . $this->auctionNode->nid), $this->getUrl());

    // Login as customer B.
    $this->drupalLogin($this->customerB);

    // Customer B places a buying order of 500, lower than customer A's original
    // buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 500);
    // Assert that system placed a bid of 55 for customer B.
    $this->assertPlaceBid($this->auctionNode->nid, 55, $this->customerB->uid, FALSE);

    // Assert that only two bids are placed now.
    $bids = array_reverse($this->getAuctionFromNode($this->auctionNode->nid, TRUE)
      ->getBids('standard')
    );
    $this->assertBidValues($bids[0], 50, $this->customerA->uid);
    $this->assertBidValues($bids[1], 55, $this->customerB->uid);
    $this->assertFalse(isset($bids[2]), 'There are 2 bids in total.');

    // Assert that only the buying order from customer B exists now.
    $buying_orders = $this->getAuctionFromNode($this->auctionNode->nid, TRUE)->getBids('buying_order');
    $this->assertEqual(1, count($buying_orders), 'Only one buying order exists.');

    // And assert that customer A received the expected mail.
    $this->assertMailSent('Your buying order has been outbid', $this->customerA->mail, 0);
    $this->assertMailSent('You have been outbid', $this->customerA->mail, 1);

    // Let auction end.
    $this->endAuctionByNode($this->auctionNode);
    // Assert that customer B won auction with a bid of 55.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $this->assertEqual($this->customerB->uid, $auction->getWinningUserId());
    $this->assertEqual(55, Auction::getController()->determineFinishedAuctionPrice($auction));
  }

  /**
   * Tests error message when a bid is placed when someone is trying to remove
   * his/her buying order.
   */
  public function testRemoveBuyingOrderWhenOverbid() {
    // Create a buying order.
    $this->placeBuyingOrder($this->auctionNode->nid, 500);
    // Assert that system placed a bid of 50 for customer A.
    $this->assertPlaceBid($this->auctionNode->nid, 50, $this->customerA->uid, FALSE);

    // Go to the page to remove it.
    $this->clickLink(t('Remove buying order'));

    // Create a bid for customer B above buying order.
    $auction = $this->getAuctionFromNode($this->auctionNode->nid);
    $bid = entity_create('auction_bid', array(
      'auction_id' => $auction->identifier(),
      'amount' => 90000,
      'currency_code' => 'EUR',
      'uid' => $this->customerB->uid,
      'type' => 'standard',
      'source' => 'standard',
    ));
    Auction::getController()->saveBid($bid);

    // Now confirm removing the buying order.
    $this->drupalPost(NULL, array(), t('Confirm'));

    // Assert error message and current path.
    $this->assertText(t('Could not remove your buying order, because new bids have been made in the mean time.'));
    $this->assertEqual($this->getAbsoluteUrl('node/' . $this->auctionNode->nid), $this->getUrl());
  }

}
