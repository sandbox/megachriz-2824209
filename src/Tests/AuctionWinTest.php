<?php

namespace Drupal\auction\Tests;

/**
 * Tests cases for when an user wins an auction.
 *
 * @group auction
 */
class AuctionWinTest extends TestBase {
  /**
   * An auction node.
   *
   * @var object
   */
  protected $auctionNode;

  /**
   * A customer who may bid and checkout.
   *
   * @var object
   */
  protected $customer;

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Auction Win Test',
      'description' => 'Tests cases for when an user wins an auction.',
      'group' => 'Auction',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp(array $modules = array(), array $permissions = array()) {
    parent::setUp($modules, $permissions);

    // Create auction node with auction.
    $this->auctionNode = $this->createNodeWithAuction(array(
      'starting_price' => 50,
    ));
    $this->customer = $this->createUserWithPermissionHelper(array('store customer', 'auction customer'));
  }

  /**
   * Tests if a product won with an auction can be ordered.
   */
  public function testAuctionWinning() {
    // Login as customer and place a bid.
    $this->drupalLogin($this->customer);
    $this->placeBid($this->auctionNode->nid, 60);

    // Let the auction end.
    $this->endAuctionByNode($this->auctionNode);

    // Run cron to place the auction product in the cart.
    $this->cronRun();

    // Assert that the product is actually in the cart.
    $order = $this->loadCommerceCartOrder($this->customer->uid);
    $product = entity_load_single('commerce_product', 1);
    $this->assertAuctionProductAddedToCart($order, $product, $this->customer);

    // Assert that product quantity in cart is not editable.
    $this->assertNoFieldByName('edit_quantity[0]');
    // Assert that the delete button is hidden.
    $xpath = $this->buildXPathQuery('//input[@name=:value]', array(':value' => 'delete-line-item-0'));
    $this->assertNoFieldByXpath($xpath);

    // And checkout.
    $this->checkout();

    // Ensure the cart is empty now.
    $this->drupalGet('cart');
    $this->assertText('Your shopping cart is empty.');

    // Run cron again and assert that the product isn't added to the cart again.
    $this->cronRun();
    $this->assertFalse($this->loadCommerceCartOrder($this->customer->uid));
    $this->drupalGet('cart');
    $this->assertText('Your shopping cart is empty.');
  }

}
