<?php

namespace Drupal\auction\Tests;

use \Drupal\auction\Auction;

/**
 * Tests if bids can be made on auctions.
 *
 * @group auction
 */
class BidTest extends TestBase {
  /**
   * An auction node.
   *
   * @var object
   */
  protected $auctionNode;

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Bid Test',
      'description' => 'Tests if bids can be made on auctions.',
      'group' => 'Auction',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp(array $modules = array(), array $permissions = array()) {
    parent::setUp($modules, $permissions);

    // Create auction node with auction.
    $this->auctionNode = $this->createNodeWithAuction(array(
      'starting_price' => 50,
    ));
  }

  /**
   * Basic test for placing a bid.
   */
  public function testPlaceBid() {
    // Login as user who may bid.
    $customer = $this->createUserWithPermissionHelper(array('store customer', 'auction customer'));
    $this->drupalLogin($customer);

    // Place a bid.
    $this->placeBid($this->auctionNode->nid, 60);
  }

  /**
   * Tests if a bid can be placed against the starting price.
   */
  public function testPlaceBidStartPrice() {
    // Login as user who may bid.
    $customer = $this->createUserWithPermissionHelper(array('store customer', 'auction customer'));
    $this->drupalLogin($customer);

    // Place a bid.
    $this->placeBid($this->auctionNode->nid, 50);
  }

  /**
   * Tests bid increment.
   */
  public function testBidIncrement() {
    // Set bid increment and minimum bid increase to 5.
    variable_set('auction_bid_step', 5);

    // Try to bid 12 more as the starting price. Should fail.
    $customer = $this->createUserWithPermissionHelper(array('store customer', 'auction customer'));
    $this->drupalLogin($customer);
    $this->placeBid($this->auctionNode->nid, 62, FALSE, FALSE);
    // Assert that the bid was not saved.
    $this->assertText(format_string('Your bid amount should increase the current high bid (@amount) by a multiple of 5.', array(
      '@amount' => Auction::formatPrice(50, 'EUR'),
    )));
    $this->assertNoPlaceBid(1);

    // Try to bid lower than the starting price with the right increment. Should fail.
    $this->placeBid($this->auctionNode->nid, 45, FALSE, FALSE);
    $this->assertText(format_string('Your bid amount is too low. The minimum bid is @amount.', array(
      '@amount' => Auction::formatPrice(50, 'EUR'),
    )));
    $this->assertNoPlaceBid(1);

    // Now try to bid with the right increment.
    $this->placeBid($this->auctionNode->nid, 55);
  }

  /**
   * Tests bid increment using two bids.
   */
  public function testBidIncrementTwoBids() {
    // Set bid increase to 10.
    variable_set('auction_bid_step', 10);

    // Create a first bid as customer 1.
    $customer1 = $this->createUserWithPermissionHelper(array('store customer', 'auction customer'));
    $this->drupalLogin($customer1);
    $this->placeBid($this->auctionNode->nid, 60);

    // Now try to bid only 1 more as customer 2. Should fail.
    $customer2 = $this->createUserWithPermissionHelper(array('store customer', 'auction customer'));
    $this->drupalLogin($customer2);
    $this->placeBid($this->auctionNode->nid, 61, FALSE, FALSE);
    // Assert that the bid was not saved.
    $this->assertText(format_string('Your bid amount is too low. The minimum bid is @amount.', array(
      '@amount' => Auction::formatPrice(70, 'EUR'),
    )));
    $this->assertNoPlaceBid(2);

    // Try to bid the start price (which is lower). Should fail.
    $this->placeBid($this->auctionNode->nid, 50, FALSE, FALSE);
    // Assert that the bid was not saved.
    $this->assertText(format_string('Your bid amount is too low. The minimum bid is @amount.', array(
      '@amount' => Auction::formatPrice(70, 'EUR'),
    )));
    $this->assertNoPlaceBid(2);

    // Finally bid the right price higher.
    $this->placeBid($this->auctionNode->nid, 70);
  }

}
