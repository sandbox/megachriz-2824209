<?php

namespace Drupal\auction\Tests;

use \stdClass;
use \CommerceBaseTestCase;
use \Drupal\auction\Auction as AuctionLib;
use \Drupal\auction\Entity\Auction;
use \Drupal\auction\Entity\Bid;

/**
 * Base class for Auction tests.
 */
abstract class TestBase extends CommerceBaseTestCase {
  /**
   * The number of seconds in one day.
   *
   * Useful for setting auction timeouts.
   *
   * @var int
   */
  const DAY_IN_SECONDS = 86400;

  /**
   * An account with admin rights.
   *
   * @var object
   */
  protected $adminAccount;

  /**
   * Product type definition.
   *
   * @var array
   */
  protected $productType;

  /**
   * Content ype.
   *
   * @var object
   */
  protected $contentType;

  /**
   * Overrides DrupalWebTestCase::setUp().
   *
   * @param array $modules
   *   (optional) A list of modules to enable.
   * @param array $permissions
   *   (optional) The permissions for the admin user.
   */
  public function setUp(array $modules = array(), array $permissions = array()) {
    $modules = array_merge($modules, $this->setUpHelper('ui'), array('psr0', 'auction', 'auction_node', 'auction_commerce', 'yvklibrary'));
    parent::setUp($modules);

    // Use permissions builder.
    $permissions = array_unique(array_merge($permissions, $this->permissionBuilder(array(
      'site admin',
      'store admin',
      'auction admin',
    ))));

    // Create an admin user.
    $this->adminAccount = $this->drupalCreateUser($permissions);
    $this->drupalLogin($this->adminAccount);

    // Add a product type.
    //$this->productType = $this->createDummyProductType('auction', 'Auction Product', '', '', FALSE);

    // Assert that the product type is shown in the UI.
    $this->drupalGet('admin/commerce/products/types');
    $this->assertText('(Machine name: auction)');
  }

  /**
   * Overrides CommerceBaseTestCase::permissionBuilder().
   *
   * Adds sets for auction permissions.
   */
  protected function permissionBuilder($sets) {
    if (is_string($sets)) {
      $sets = array($sets);
    }

    $auction_admin = array(
      'administer auctions',
      'create auctions',
      'create auction content',
      'edit any auction content',
      'delete any auction content',
      'view bids',
      'auction_bid.buying_order.delete',
    );
    $auction_editor = array(
      'create auctions',
      'create auction content',
      'edit own auction content',
      'delete own auction content',
    );
    $auction_customer = array(
      'bid in auctions',
      'view auctions',
      'view bids',
      'auction_bid.buying_order.delete.own',
    );

    $final_permissions = parent::permissionBuilder($sets);

    foreach ($sets as $set) {
      switch ($set) {
        case 'auction admin':
          $final_permissions = array_unique(array_merge($final_permissions, $auction_admin));
          break;

        case 'auction editor':
          $final_permissions = array_unique(array_merge($final_permissions, $auction_editor));
          break;

        case 'auction customer':
          $final_permissions = array_unique(array_merge($final_permissions, $auction_customer));
          break;
      }
    }

    return $final_permissions;
  }

  /**
   * Creates an auction.
   *
   * @param array $values
   *   (optional) Values to use for the auction.
   *
   * @return \Drupal\auction\Entity\Auction
   *   An auction instance.
   */
  protected function createAuction(array $values = array()) {
    $controller = AuctionLib::getController();
    $auction = $controller->create();

    $auction->field_auction_date[LANGUAGE_NONE][0]['value'] = time() - 1;
    $auction->field_auction_date[LANGUAGE_NONE][0]['value2'] = time() + 86400;

    foreach ($values as $key => $value) {
      switch ($key) {
        case 'starting_price':
        case 'buy_now_price':
        case 'minimum_price':
        case 'bid_step':
        case 'date':
          $field_name = 'field_auction_' . $key;
          $auction->{$field_name}[LANGUAGE_NONE][0]['value'] = $value;
          break;

        case 'date_start':
          $auction->field_auction_date[LANGUAGE_NONE][0]['value'] = $value;
          break;

        case 'date_end':
          $auction->field_auction_date[LANGUAGE_NONE][0]['value2'] = $value;
          break;
      }

    }

    $auction->save();

    return $auction;
  }

  /**
   * Creates a node linked to an auction.
   */
  protected function createAuctionNode($auction_id) {
    $node = new stdClass();
    $node->type = 'auction';
    $node->title = $this->randomName();
    $node->field_auction[LANGUAGE_NONE][0]['target_id'] = $auction_id;
    $node->uid = 1;
    node_save($node);
    return $node;
  }

  /**
   * Creates a node and auction.
   *
   * @param array $values
   *   (optional) Values to use for the auction.
   */
  protected function createNodeWithAuction(array $values = array()) {
    $auction = $this->createAuction($values);
    return $this->createAuctionNode($auction->auction_id);
  }

  /**
   * Removes lock from commerce order entities.
   */
  protected function releaseCommerceOrderLock() {
    entity_get_controller('commerce_order')->resetCache();
  }

  /**
   * Loads a single commerce order for the given user without lock.
   *
   * @param int $uid
   *   The user to load a cart order for.
   *
   * @return object
   *   A commerce order, if found.
   *   False otherwise.
   */
  protected function loadCommerceCartOrder($uid) {
    drupal_static_reset('commerce_cart_order_id');
    $order = commerce_cart_order_load($uid);
    // Reset the cache as we don't want to keep the lock.
    $this->releaseCommerceOrderLock();

    return $order;
  }

  /**
   * Loads a single commerce order without lock.
   *
   * @param int $order_id
   *   ID of the order to load.
   *
   * @return object
   *   A commerce order, if found.
   *   False otherwise.
   */
  protected function loadCommerceOrder($order_id, $reset = FALSE) {
    $orders = commerce_order_load_multiple(array($order_id), array(), $reset);
    $order = reset($orders);
    // Reset the cache as we don't want to keep the lock.
    $this->releaseCommerceOrderLock();

    return $order;
  }

  /**
   * Reloads a single auction.
   *
   * @param \Drupal\auction\Entity\Auction $auction
   *   The auction to reload.
   */
  protected function reloadAuction($auction) {
    // Reload any bids as well.
    entity_get_controller('auction_bid')->resetCache();
    $auctions = entity_load('auction', array($auction->auction_id), array(), TRUE);
    return reset($auctions);
  }

  /**
   * Gets an auction from a node.
   *
   * @param int|object $node
   *   Either a node ID or a full node object.
   * @param bool $reload
   *   (optional) Whether or not to reload the auction.
   *   Defaults to false.
   *
   * @return \Drupal\auction\Entity\Auction
   *   An auction object, if found.
   *   Null otherwise.
   */
  protected function getAuctionFromNode($node, $reload = FALSE) {
    $auction = entity_metadata_wrapper('node', $node)
      ->field_auction
      ->value();
    if ($auction && $reload) {
      return $this->reloadAuction($auction);
    }
    return $auction;
  }

  /**
   * Places a bid.
   *
   * @param int $nid
   *   ID of the auction node to place a bid on.
   * @param float $amount
   *   The amount to bid.
   * @param bool $confirm
   *   (optional) Whether or not to confirm the bid.
   *   Defaults to TRUE.
   * @param bool $assert_success
   *   (optional) Whether or not to assert success of the action.
   *   Defaults to TRUE.
   */
  protected function placeBid($nid, $amount, $confirm = TRUE, $assert_success = TRUE) {
    $edit = array(
      'bid_amount' => $amount,
      'form_id' => 'auction_bids_form',
    );
    $this->drupalPost('node/' . $nid, $edit, t('Place bid'));

    if ($confirm) {
      $this->drupalPost(NULL, array(), t('Confirm'));
    }
    if ($assert_success) {
      $this->assertPlaceBid($nid, $amount);
    }
  }

  /**
   * Ends an auction by changing the auction timeout to one second in the past.
   *
   * @param \Drupal\auction\Entity\Auction $auction
   *   The auction to end.
   */
  protected function endAuction(Auction $auction) {
    $auction->field_auction_date[LANGUAGE_NONE][0]['value2'] = REQUEST_TIME -1;
    $auction->save();
  }

  /**
   * Ends an auction for an auction node.
   *
   * @param object $node
   *   The auction node to end.
   */
  protected function endAuctionByNode($node) {
    $this->endAuction($this->getAuctionFromNode($node));
  }

  /**
   * Perform a checkout.
   *
   * Taken from CommerceCheckoutTestProcess::testCommerceCheckoutProcessAuthenticatedUser().
   *
   * @see CommerceCheckoutTestProcess::testCommerceCheckoutProcessAuthenticatedUser()
   */
  protected function checkout() {
    // Load existing cart order.
    $order = $this->loadCommerceCartOrder($this->loggedInUser->uid);
    if (!$order) {
      $this->fail('Failed to checkout. No cart order exists.');
      return;
    }

    $this->drupalGet($this->getCommerceUrl('checkout'));

    // Generate random information, as city, postal code, etc.
    $address_info = $this->generateAddressInformation();

    // Check if the page resolves and if the default panes are present.
    $this->assertResponse(200, t('The owner of the order can access to the checkout page'));
    $this->assertTitle(t('Checkout') . ' | Drupal', t('Title of the checkout phase is correct'));
    $this->assertText(t('Shopping cart contents'), t('Shopping cart contents pane is present'));
    $this->assertText(t('Billing information'), t('Billing information pane is present'));

    // We are testing with authenticated user, so no account information
    // should appear.
    $this->assertNoText(t('Account information'), t('Account information pane is not present'));

    // Fill in the billing address information.
    $billing_pane = $this->xpath("//select[starts-with(@name, 'customer_profile_billing[commerce_customer_address]')]");
    $this->drupalPostAJAX(NULL, array((string) $billing_pane[0]['name'] => 'US'), (string) $billing_pane[0]['name']);

    // Check if the country has been selected correctly, this uses XPath as the
    //  ajax call replaces the element and the id may change.
    $this->assertFieldByXPath("//select[starts-with(@id, 'edit-customer-profile-billing-commerce-customer-address')]//option[@selected='selected']", 'US', t('Country selected'));

    // Fill in the required information for billing pane, with a random State.
    $info = array(
      'customer_profile_billing[commerce_customer_address][und][0][name_line]' => $address_info['name_line'],
      'customer_profile_billing[commerce_customer_address][und][0][thoroughfare]' => $address_info['thoroughfare'],
      'customer_profile_billing[commerce_customer_address][und][0][locality]' => $address_info['locality'],
      'customer_profile_billing[commerce_customer_address][und][0][administrative_area]' => 'KY',
      'customer_profile_billing[commerce_customer_address][und][0][postal_code]' => $address_info['postal_code'],
    );
    $this->drupalPost(NULL, $info, t('Continue to next step'));

    // Check for default panes and information in this checkout phase.
    $this->pass(t('Checking the default panes and the page information:'));
    $this->assertTitle(t('Review order') . ' | Drupal', t('Title of the checkout phase \'Review order\' is correct'));
    $this->assertText($address_info['name_line'], t('Billing information for \'name_line\' is correct'));
    $this->assertText($address_info['thoroughfare'], t('Billing information for \'thoroughfare\' is correct'));
    $this->assertText($address_info['locality'], t('Billing information for \'locality\' is correct'));
    $this->assertText(trim($address_info['postal_code']), t('Billing information for \'postal_code\' is correct'));
    $this->assertText('United States', t('Billing information country is correct'));

    // Reload the order to check the status.
    $order = $this->loadCommerceOrder($order->order_id, TRUE);

    // At this point we should be in Checkout Review.
    $this->assertEqual($order->status, 'checkout_review', t('Order status is \'Checkout Review\' in the review phase.'));

    // Finish checkout process.
    $this->drupalPost(NULL, array(), t('Continue to next step'));

    // Reload the order directly from db to update status.
    $order = $this->loadCommerceOrder($order->order_id, TRUE);

    // Order status should be pending when completing checkout process.
    $this->assertEqual($order->status, 'pending', t('Order status is \'Pending\' after completing checkout'));
    // Check if the completion message has been displayed.
    $this->assertTitle(t('Checkout complete') . ' | Drupal', t('Title of the page is \'Checkout complete\' when finishing the checkout process'));
    $this->assertText(t('Your order number is @order-number.', array('@order-number' => $order->order_number)), t('Completion message for the checkout is correctly displayed'));
  }

  /**
   * Asserts that a bid was saved.
   *
   * @param int $nid
   *   ID of the auction node were a bid was placed for.
   * @param float $amount
   *   The expected amount that was bid.
   * @param int $uid
   *   (optional) The expected user who placed the bid.
   *   Defaults to currently logged in user.
   * @param bool $assert_message
   *   (optional) If expected that a message is displayed.
   *   Defaults to TRUE.
   */
  protected function assertPlaceBid($nid, $amount, $uid = NULL, $assert_message = TRUE) {
    $auction = $this->getAuctionFromNode($nid, TRUE);
    $bids = $auction->getBids('standard');
    $bid = reset($bids);

    $this->assertBidValues($bid, $amount, $uid);

    // Assert that it is displayed as well.
    if ($assert_message) {
      $this->assertText(t('Your bid has been placed.'));
      $this->assertText(AuctionLib::formatPrice($amount, 'EUR'));
    }
  }

  /**
   * Asserts that a bid has the expected amount and owner.
   *
   * @param \Drupal\auction\Entity\Bid $bid
   *   The bid to check for values.
   * @param float $amount
   *   The expected amount that was bid.
   * @param int $uid
   *   (optional) The expected user who placed the bid.
   *   Defaults to currently logged in user.
   */
  protected function assertBidValues($bid, $amount, $uid = NULL) {
    $amount_in_cents = $amount * 100;

    // Check if a bid is actually given.
    if (!($bid instanceof Bid)) {
      // No bid given!
      $this->fail(format_string('The bid was placed with an amount of @amount (actual: @actual).', array(
        '@amount' => $amount_in_cents,
        '@actual' => 'NONE',
      )));
      return;
    }

    // Assert that the bid has the expected amount.
    $this->assertEqual($amount_in_cents, $bid->amount, format_string('The bid was placed with an amount of @amount (actual: @actual).', array(
      '@amount' => $amount_in_cents,
      '@actual' => $bid->amount,
    )));

    if (is_null($uid)) {
      $uid = $this->loggedInUser->uid;
    }
    $this->assertEqual($uid, $bid->uid, format_string('The bid was placed by user @uid (actual: @actual).', array(
      '@uid' => $uid,
      '@actual' => $bid->uid,
    )));
  }

  /**
   * Asserts that a bid was *not* saved.
   *
   * @param int $bid_id
   *   The ID of the bid to check.
   */
  protected function assertNoPlaceBid($bid_id) {
    $count = db_select('auction_bids', 'ab')
      ->fields('ab', array('bid_id'))
      ->condition('bid_id', $bid_id)
      ->countQuery()
      ->execute()
      ->fetchField();

    $this->assertEqual(0, $count, 'The bid was not saved.');
  }

  /**
   * Asserts that a auction product has been added to the cart.
   *
   * @param object $order
   *   A full loaded commerce_order object.
   * @param object  $product
   *   A full loaded commerce_product object.
   *
   * @return TRUE if the product is in the cart, FALSE otherwise.
   */
  public function assertAuctionProductAddedToCart($order, $product) {
    // The order should be in cart status.
    $this->assertTrue(commerce_cart_order_is_cart($order), t('The order checked is in cart status'));

    $product_is_in_cart = FALSE;
    // Loop through the line items looking for products.
    foreach (entity_metadata_wrapper('commerce_order', $order)->commerce_line_items as $delta => $line_item_wrapper) {
      // If this line item matches the product checked...
      if ($line_item_wrapper->getBundle() == 'product' &&
          $line_item_wrapper->commerce_product->product_id->value() == $product->product_id) {
            $product_is_in_cart = TRUE;
      }
    }

    $this->assertTrue($product_is_in_cart, t('Product !product_title is present in the cart', array('!product_title' => $product->title)));

    // Access to the cart page to check if the product is there.
    $this->drupalGet($this->getCommerceUrl('cart'));
    $this->assertText($product->title, t('Product !product_title is present in the cart view', array('!product_title' => $product->title)));
  }

  /**
   * Asserts that a field in the current page is disabled.
   *
   * @param string $name
   *   Name of field to assert.
   * @param string $message
   *   (optional) A message to display with the assertion. Do not translate
   *   messages: use \Drupal\Component\Utility\SafeMarkup::format() to embed
   *   variables in the message text, not t(). If left blank, a default message
   *   will be displayed.
   *
   * @return bool
   *   TRUE on pass, FALSE on fail.
   */
  protected function assertFieldDisabled($name, $message = '') {
    $elements = $this->xpath($this->constructFieldXpath('name', $name));
    return $this->assertTrue(isset($elements[0]) && !empty($elements[0]['disabled']), $message ? $message : t('Field %name is disabled.', array('%name' => $name)), t('Browser'));
  }

}
