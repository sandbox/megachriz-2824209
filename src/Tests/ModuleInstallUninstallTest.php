<?php

namespace Drupal\auction\Tests;

/**
 * Tests module installation and uninstallation.
 *
 * @group auction
 */
class ModuleInstallUninstallTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Module installation and uninstallation',
      'description' => 'Tests if the Commerce Auction module can be installed and uninstalled without errors.',
      'group' => 'Auction',
    );
  }

  /**
   * Test installation and uninstallation.
   */
  protected function testInstallationAndUninstallation() {
    $this->assertTrue(module_exists('auction'));
    $this->assertTrue(module_exists('auction_node'));
    $this->assertTrue(module_exists('auction_commerce'));

    // @todo
    // Check availability of things.

    // Uninstall module.
    module_disable(array('auction', 'auction_commerce'));
    drupal_uninstall_modules(array('auction', 'auction_commerce'));
    $this->assertFalse(module_exists('auction'));
    $this->assertFalse(module_exists('auction_node'));
    $this->assertFalse(module_exists('auction_commerce'));

    // @todo
    // Check if all is properly cleaned up.
  }
}
