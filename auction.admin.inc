<?php

/**
 * May 27, 2012
 * @file auction.admin.inc
 * @author blazey http://drupal.org/user/353861
 */

use \Drupal\auction\Auction;
use \Drupal\auction\Entity\Bid;

/**
 * Module settings form.
 *
 * @return array
 */
function auction_settings_form() {
  $settings = Auction::getSettings();

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );
  $form['general']['auction_currency'] = array(
    '#type' => 'textfield',
    '#title' => t('Currency code'),
    '#default_value' => $settings['currency'],
    '#required' => TRUE,
    '#maxlength' => 3,
    '#size' => 5,
  );
  $form['general']['auction_bid_step'] = array(
    '#type' => 'textfield',
    '#title' => t('Bid step'),
    '#default_value' => $settings['bid_step'],
    '#required' => TRUE,
    '#size' => 5,
  );

  $form['auction_timeout_extend'] = array(
    '#type' => 'fieldset',
    '#title' => t('Auction Timeout settings'),
    '#description' => t('All times are measured in minutes'),
  );
  $form['auction_timeout_extend']['auction_extend'] = array(
    '#type' => 'checkbox',
    '#title' => t('Extend auction time if there is a new bid in the last few minutes'),
    '#default_value' => variable_get('auction_extend', TRUE),
  );
  $form['auction_timeout_extend']['auction_final_period'] = array(
    '#type' => 'textfield',
    '#title' => t('Extend if a new bid is placed in'),
    '#default_value' => variable_get('auction_final_period', 15),
    '#states' => array(
      'visible' => array(
        ':input[name="auction_extend"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['auction_timeout_extend']['auction_extension_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Extend the auction time for'),
    '#default_value' => variable_get('auction_extension_time', 30),
    '#states' => array(
      'visible' => array(
        ':input[name="auction_extend"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['buy_now'] = array(
    '#type' => 'fieldset',
    '#title' => t('Buy now settings'),
  );
  $form['buy_now']['auction_buy_now_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Disable buy now when'),
    '#default_value' => $settings['buy_now_mode'],
    '#options' => array(
      'first bid' => t('first bid is placed.'),
      'percentage' => t('auction price reaches threshold.'),
    ),
  );
  $form['buy_now']['auction_buy_now_threshold'] = array(
    '#type' => 'textfield',
    '#title' => t('Threshold'),
    '#default_value' => $settings['buy_now_threshold'],
    '#required' => FALSE,
    '#size' => 5,
    '#field_suffix' => t('% of buy now price.'),
  );

  $form['fees'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fees'),
  );
  $form['fees']['auction_usage_fee'] = array(
    '#type' => 'textfield',
    '#title' => t('Usage fee'),
    '#default_value' => $settings['usage_fee'],
    '#required' => TRUE,
    '#size' => 5,
  );
  $form['fees']['auction_provision_fee'] = array(
    '#type' => 'textfield',
    '#title' => t('Provision fee'),
    '#default_value' => $settings['provision_fee'],
    '#required' => TRUE,
    '#size' => 5,
  );


  return system_settings_form($form);
}

/**
 * Form for editing a bid.
 */
function auction_bid_admin_form($form, &$form_state, Bid $bid) {
  $entity_type = $bid->entityType();

  $form['bid_amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#default_value' => $bid->amount / 100,
    '#required' => TRUE,
    '#field_suffix' => $bid->currency_code,
    '#size' => 10,
  );

//  field_attach_form($entity_type, $bid, $form, $form_state);

  // Author information for administrators.
  if ($bid->uid) {
    $user = user_load($bid->uid);
    $bid->bidder = $user->name;
  }
  $form['bidder'] = array(
    '#type' => 'textfield',
    '#title' => t('Bidder'),
    '#access' => user_access('administer auctions'),
    '#maxlength' => 60,
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => !empty($bid->bidder) ? $bid->bidder : '',
    '#weight' => -1,
    '#description' => t('Leave blank for %anonymous.', array('%anonymous' => variable_get('anonymous', t('Anonymous')))),
  );

  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#default_value' => $bid->status,
  );

  // Actions.
  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // Save button.
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form API submit callback for the yvkentity entity form.
 */
function auction_bid_form_submit(&$form, &$form_state) {
  $entity = $form_state['auction_bid'];
  entity_form_submit_build_entity('auction_bid', $entity, $form, $form_state);

  // Set amount.
  if (isset($entity->bid_amount)) {
    $entity->amount = $entity->bid_amount * 100;
  }

  // Set bidder.
  if (isset($entity->bidder)) {
    if ($account = user_load_by_name($entity->bidder)) {
      $entity->uid = $account->uid;
    }
    else {
      $entity->uid = 0;
    }
  }

  $entity->save();

  drupal_set_message(t('Bid saved.'));
}
