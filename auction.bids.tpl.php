<?php

/**
 * @file
 * Template for bidding.
 */
?>
<div id="auction-bids-<?php print $auction->identifier(); ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php if (!empty($content['form'])) : ?>
    <?php print drupal_render($content['form']); ?>
  <?php endif; ?>

  <?php if (!empty($content['instant_buy'])) : ?>
    <?php print $content['instant_buy']; ?>
  <?php endif; ?>

  <?php if (!empty($content['history'])) : ?>
    <?php print $content['history']; ?>
  <?php endif; ?>
</div>