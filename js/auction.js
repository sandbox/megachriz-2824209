(function($) {

/**
 * Disable the confirm button once it is clicked and provide a notification to
 * the user.
 */
Drupal.behaviors.auctionConfirmBid = {
  attach: function (context, settings) {
    // When the button to confirm a bid is clicked we disable it so it is not
    // accidentally clicked twice.
    $('form#auction-bids-form.confirmation input.form-submit:not(.form-submit-processed)', context).addClass('form-submit-processed').click(function() {
      $('form#auction-bids-form').prepend('<div class="messages status">' + Drupal.t('Your bid will now be placed.') + '</div>');

      var $this = $(this);
      $this.clone().insertAfter(this).attr('disabled', true).next().removeClass('element-invisible');
      $this.hide();
    });
  }
}

})(jQuery);
