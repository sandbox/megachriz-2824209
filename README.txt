AUCTION
=======
Allows selling nodes with auctions.

Hidden settings
===============

Hidden settings are variables that you can define by adding them to the $conf
array in your settings.php file.

Name:        auction_rules_active
Default:     TRUE
Description: When set to FALSE all default rules by this module get disabled
             (if not overridden). This is handy if you want to disable them all
             and don't want the rules to appear as overridden.
