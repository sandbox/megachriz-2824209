<?php

/**
 * Jun 15, 2012
 * @file auction.api.php
 * @author blazey http://drupal.org/user/353861
 */

use Drupal\auction\Entity\Auction;
use Drupal\auction\Exception\InvalidBidException;

/**
 * This hook allows you to specify the default bid step to use for the auction.
 *
 * @param \Drupal\auction\Entity\Auction $auction
 *   The auction to specify the bid step for.
 *
 * @return int|null
 *   The bid step to use for the auction.
 *   Return null to leave it to other modules to specify the bid step.
 */
function hook_auction_bid_step(Auction $auction) {
  // Use a bid step of 200 when the starting price is 5000 or higher.
  if ($auction->getStartingPrice() >= 5000) {
    return 200;
  }
}

/**
 * This hook allows you to validate a bid.
 *
 * Throw a \Drupal\auction\Exception\InvalidBidException in case the bid
 * is considered invalid.
 *
 * @param \Drupal\auction\Entity\Auction $auction
 *   The auction to check a bid amount for.
 * @param int $bid_amount
 *   The bid amount to validate.
 *
 * @throws \Drupal\auction\Exception\InvalidBidException
 *   In case of an invalid bid.
 */
function hook_auction_validate_bid(Auction $auction, $bid_amount) {
  // Example: the bid may not increase 3 times the current bid.
  if ($bid_amount >= (3 * $auction->getCurrentPrice()) {
    throw new InvalidBidException(t('Your bid may not multiply the current bid by 3 or more.'));
  }
}

/**
 * Tell auction module about you fee ranges.
 *
 * @return array
 */
function hook_auction_fee_ranges() {
  return array(
    array(
      'from' => 0,
      'sell_price_fee' => 0.03,
      'single_auction_fee' => 15,
    ),
    array(
      'from' => 10000,
      'sell-price-fee' => 0.025,
      'single_auction_fee' => 50,
    ),
    array(
      'from' => 100000,
      'sell-price-fee' => 0.02,
      'single_auction_fee' => 100,
    ),
  );
}

/**
 * Alter existing fee ranges.
 *
 * @param array &$ranges
 */
function hook_auction_fee_ranges_alter(&$ranges) {
  $ranges[1]['from'] = 15000;
}