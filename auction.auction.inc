<?php

/**
 * @file
 * Hooks by the auction module.
 */

/**
 * Implements hook_auction_bid_types().
 */
function auction_auction_bid_types() {
  $path = drupal_get_path('module', 'auction') . '/src/Plugin/auction/BidType';

  $info = array();

  // Handler for the standard bid type.
  $info['standard'] = array(
    'handler' => array(
      'class' => 'Drupal\\auction\\Plugin\\auction\\BidType\\StandardBidType',
      'file' => 'StandardBidType.php',
      'path' => $path,
    ),
  );

  // Instant buy.
  $info['instant_buy'] = array(
    'handler' => array(
      'class' => 'Drupal\\auction\\Plugin\\auction\\BidType\\InstantBuyBidType',
      'file' => 'InstantBuyBidType.php',
      'path' => $path,
    ),
  );

  // Buying order.
  $info['buying_order'] = array(
    'handler' => array(
      'class' => 'Drupal\\auction\\Plugin\\auction\\BidType\\BuyingOrderBidType',
      'file' => 'BuyingOrderBidType.php',
      'path' => $path,
    ),
  );

  return $info;
}
